
import MatrixKalkJava.*;
import java.util.Vector;
import java.util.Random;
import java.text.DecimalFormat;

public class Use {


	public static void main(String[] args) {

		try{
			System.out.println("\n   ****************************************************");
			System.out.println("************************************************************");
			System.out.println("    **------------------MATRIXKALK--------------------**    ");
			System.out.println("************************************************************");
			System.out.println("   ****************************************************\n");;

			
			System.out.println("\n------------------------------------------------------------");
			System.out.println("|                                                          |");
			System.out.println("|                      PARTE MATRICI                       |");
			System.out.println("|                                                          |");
			System.out.println("------------------------------------------------------------");

			//MATRICI	
			System.out.println("Costruzione di una matrice 4x3");
			
			Matrice a = new Matrice(4,3,0.0);
			
			

			System.out.println("Riempimento casuale di A:");
			a.randomFill();
			System.out.println("Matrice A:");
			a.stampa();
			System.out.println(" -------***********************************-------");
			System.out.println("                 OPERAZIONI UNARIE                     ");
			System.out.println(" -------***********************************-------\n");

			System.out.println("_______________________________________________________");
			System.out.println("\n                       Matrice          ");
			System.out.println("           **********************************\n");


			System.out.println("---------------Trasposta di A-----------------");
			Matrice tr=a.trasponi();
			tr.stampa();
			
			System.out.println("\n---------------Rango di A---------------------");
			int rango=a.rango();
			System.out.println(rango);
			

			System.out.println("\n---------------Moltiplicazione per Scalare----");
			double scalare = 2.5;
			System.out.println("Scalare: "+scalare);
			Matrice scal = a.multiplyForScalar(scalare);
			scal.stampa();



			System.out.println("\n---------------Decomposizione PLU di A---------");
			Vector <Matrice> plu=a.decomposizionePLU();

			System.out.println("        P");
			plu.get(0).stampa();
			System.out.println("        L");
			plu.get(1).stampa();
			System.out.println("        U");
			plu.get(2).stampa();

			System.out.println("\n---------------Eliminazione di Gauss-Jordan su A-");
			Vector<Matrice> ris = a.eliminazioneGaussJordan(); 
			
			ris.get(0).stampa();
			ris.get(1).stampa();
			System.out.println("_______________________________________________________");
			//Metodi di matrice Quadrata
			System.out.println("\n                  Matrice  Quadrata         ");
			System.out.println("           **********************************\n");
			System.out.println("Creo una nuova matrice B con TS(Matrice) e TD(MatriceQuadrata):");
			Matrice B = new MatriceQuadrata(4);
			B.randomFill();
			System.out.println("Stampa di B:");
			B.stampa();
			System.out.println("\n---------------Determinante di B---------------");
			if(B instanceof MatriceQuadrata){
				double det = ((MatriceQuadrata)B).determinante();
				System.out.println("det(B): "+det);
			}
			System.out.println("\n---------------Inversa di B---------------");

			if(B instanceof MatriceQuadrata){
				try{
					((MatriceQuadrata)B).inversa().stampa();
				}
				catch(ErroriMatrice ecc){
					System.out.println("Qualcosa è andato storto: è stata generata una matrice nulla.");
					System.out.println("Prova a far ripartire il programma, magari sarai più fortunato.");
				}
				
			}

			System.out.println("\n************Creazione di una matrice Quadrata nulla e tentativo di calcolo della sua inversa");
			MatriceQuadrata ex = new MatriceQuadrata(3);
			ex.stampa();
			System.out.println("\n****** Verra\' sollevata un'eccezione: ");
			try {
				ex.inversa();
			}
			catch(ErroriMatrice e){
				System.out.println(e.what());
			}


			//Vettore
			System.out.println("_______________________________________________________");
			System.out.println("\n                       Vettore        ");
			System.out.println("           **********************************\n        ");
			
			System.out.println("Creo un nuovo vettore colonna con TS(Matrice) e TD(Vettore):");
			Matrice V = new Vettore(4,0,false);
			V.randomFill();
			V.stampa();
			
			//Norma
			System.out.println("\n---------------Norma euclidea di V---------------");
			if(V instanceof Vettore);
				System.out.println("||V||: "+((Vettore)V).norma());
			
			System.out.println("\n---------------Normalizzazione di V--------------");
			if(V instanceof Vettore){
				try {
					((Vettore)V).normalizzazione().stampa();
				}
				catch(ErroriMatrice e) {
					System.out.println("Qualcosa è andato storto: è stata generato un Vettore che ha norma nulla.");
					System.out.println("Prova a far ripartire il programma, magari sarai più fortunato.");
				}
			}
			
			System.out.println("Creo un nuovo vettore colonna U:");
			Matrice U = new Vettore(4,0,false);
			U.randomFill();
			U.stampa();

			System.out.println("\n---------------Angolo tra V ed U----------------");
			if(V instanceof Vettore && U instanceof Vettore) {
				System.out.println(((Vettore)V).angolo(((Vettore)U),true)+"\u00b0");
				System.out.println(((Vettore)V).angolo(((Vettore)U),false)+" rad");
			}

			System.out.println("\n---------------Distanza tra V ed U--------------");
			if(V instanceof Vettore && U instanceof Vettore) {
				System.out.println("d(V,U): "+((Vettore)V).distanza(((Vettore)U)));
			}

			System.out.println("Creo un nuovo vettore riga con TS(Matrice) e TD(Vettore):");
			Matrice W = new Vettore(4,0,true);
			W.randomFill();
			W.stampa();

			System.out.println("La norma e la normalizzazione sono definite solo su vettori colonna.\n Ora si proveranno ad eseguire queste due operazioni su W, quindi verranno lanciate delle eccezioni.");
			System.out.println("\n---------------Normalizzazione di W--------------");
			if(W instanceof Vettore) {
				try{
					((Vettore)W).normalizzazione();
				}
				catch(ErroriMatrice e) {
					System.out.println(e.what());
				}
			}
			System.out.println("\n---------------Norma euclidea di V---------------");
			if(W instanceof Vettore) {
				try{
					((Vettore)W).norma();
				}
				catch(ErroriMatrice e) {
					System.out.println(e.what());
				}
			}

			//Operazioni binarie
			System.out.println(" \n-------***********************************-------");
			System.out.println("                 OPERAZIONI BINARIE                     ");
			System.out.println("-------***********************************-------\n");

			System.out.println("\n ______________Moltiplicazione_________________");

			System.out.println("\n--------------stampa di V(4x1)-----------------");
			V.stampa();

			System.out.println("\n--------------stampa di A(4x3)-----------------");
			a.stampa();

			System.out.println("\n--------------stampa di B(4x4)-----------------");
			B.stampa();

			System.out.println("\n--------------stampa di V*A (incompatibili)----");		
			System.out.println("INFO: E' stata chiamata la moltiplicazione di Vettore per polimorfismo di V");
			try{
				(V.multiply(a)).stampa();
			}
			catch(ErroriMatrice e){

				System.out.println(e.what());
			}

			System.out.println("\n--------------stampa di B*A------------------- ");
				(B.multiply(a)).stampa();


			System.out.println("\n ______________Addizione_________________");
			System.out.println("\n--------------Creazione di una nuova matrice C-----------------");
			Matrice C = new Matrice(4,3,0.0);
			C.randomFill();
			C.stampa();

			System.out.println("\n----------------A + C:------------------------ ");
			(a.add(C)).stampa();

			System.out.println("\n----------------A + V (eccezione) ------------ ");
			try{
				a.add(V);
			}
			catch(ErroriMatrice e) {
				System.out.println(e.what());
			}
			System.out.println("\n ________________Sottrazione___________________");
			System.out.println("\n----------------A - C:------------------------ ");
			(a.add(C)).stampa();

			System.out.println("\n________________Concatenazione di A e B________________");
			a.concatena(B).stampa();


			System.out.println("\n------------------------------------------------------------");
			System.out.println("|                                                          |");
			System.out.println("|                      PARTE COMPLESSI                     |");
			System.out.println("|                                                          |");
			System.out.println("------------------------------------------------------------");



			System.out.println("\n___Creazione  e stampa di un nuovo complesso (A)___");
			DecimalFormat df= new DecimalFormat("#.##");
			Random generator = new Random();
			Complesso compl= new Complesso((-50.0) + ((generator.nextDouble()*100.0)),(-50.0) + (generator.nextDouble()*100.0));
			compl.stampa();

			System.out.println(" -------***********************************-------");
			System.out.println("                 OPERAZIONI UNARIE                     ");
			System.out.println(" -------***********************************-------\n");


			System.out.println("\n_________________Argomento___________________ \n"+df.format(compl.argomento()));

			System.out.println("\n__________________Modulo_____________________\n"+df.format(compl.modulo()));

			System.out.println("\n_________________Coniugato___________________");
			compl.coniugato().stampa();

			System.out.println("\n____________Potenza con esponente 2__________");
			compl.pow(2).stampa();

			System.out.println("\n______________Calcolo di 4 radici_____________");
			Complesso[] roots=compl.root(4);

			for(int i=0; i< roots.length; i++) {
				int k=i+1;
				System.out.println("\nRadice "+k+" =\n");
				roots[i].stampa();
			}

			System.out.println(" \n-------***********************************-------");
			System.out.println("                 OPERAZIONI BINARIE                     ");
			System.out.println("-------***********************************-------\n");
		


			System.out.println("\n___Creazione  e stampa di un secondo complesso (B)___");
			Complesso operand= new Complesso((-50.0) + ((generator.nextDouble()*100.0)),(-50.0) + (generator.nextDouble()*100.0));
			operand.stampa();


			System.out.println("\n _________________Addizione- A+B_________________");
			compl.add(operand).stampa();

			System.out.println("\n ______________Moltiplicazione- A*B_________________");
			compl.multiply(operand).stampa();

			System.out.println("\n ________________Sottrazione- A-B___________________");
			compl.subtract(operand).stampa();

			System.out.println("\n _________________Divisione- A-B__________________");
			compl.divide(operand).stampa();
		}
		catch(ErroriMatrice e) {
			System.out.println("Si e\' verificato un errore durante l'esecuzione del programma:");
			System.out.println(e.what());
		}
	
		catch(ErroriComplesso e) {
			System.out.println("Si e\' verificato un errore durante l'esecuzione del programma:");
			System.out.println(e.what());

		}


	}
}
