package MatrixKalkJava;

public class ErroriMatrice extends Exception{
	
	private String err="si e\' verificato un errore non previsto";

	public ErroriMatrice(String text) {
        super(text);
        err= text;
    }

    public ErroriMatrice(String text, Throwable exc) {
        super(text, exc);
    	err = text;
    }

    public ErroriMatrice(Throwable exc) {
    	super(exc);
    }


    public String what(){
    	return err;

    }
}