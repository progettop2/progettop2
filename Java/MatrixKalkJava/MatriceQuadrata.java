package MatrixKalkJava;

import java.util.Vector;

public class MatriceQuadrata extends Matrice {

	public MatriceQuadrata(double[][] a, int dimensione) {
		super(a,dimensione,dimensione);
	}

	public MatriceQuadrata(double [][] a){
		super(a,1,1);

	}


	public MatriceQuadrata() {
		super(1,1,0.0);

	}

	public MatriceQuadrata(int dimensione) {

		super(dimensione,dimensione,0.0);
	}


	public MatriceQuadrata(int dimensione, double val) {

		super(dimensione,dimensione,val);
	}

	public MatriceQuadrata(MatriceQuadrata m) {

		super(m.getElementi(),m.getRighe(),m.getColonne());
	}

	public MatriceQuadrata matriceIdentita() {

    MatriceQuadrata mc= new MatriceQuadrata(getColonne(),0.0);

    for(int j=0; j < mc.getColonne(); ++j){
        mc.setEl(1,j,j);
    }
    return mc;

	}

	public double determinante2x2() throws ErroriMatrice {
		return (getEL(0,0)*getEL(1,1)) - (getEL(0,1)*getEL(1,0));
	}

	public double determinante() throws ErroriMatrice {
    if(getColonne() == 2)
        return determinante2x2();
    else {
	        int permutazioni = 0;
	        double moltiplicatore = 1.0;
	        Vector<Matrice> PLU = decomposizionePLU();
	        int dimMperm = (PLU.get(0)).getRighe();
	        for(int i = 0; i < dimMperm; ++i) {
	            if((PLU.get(0).getEL(i,i)) == 0 )
	                permutazioni++;
        }

	        Matrice U = new Matrice(PLU.get(2));
	        int n = getColonne();
	        double det = 1.0;
		        for(int i = 0; i < n; ++i) {
		           det = det*(U).getEL(i,i);
	    	    }
	        	if(permutazioni%2 != 0){
	            	moltiplicatore = -1.0;
	        	}
	        	return moltiplicatore * det;
    	}
	}


	public boolean checkInvertibile() {
    	return rango() == getColonne();
	}

	public MatriceQuadrata inversa() throws ErroriMatrice {
    if(!checkInvertibile()) {
        throw new ErroriMatrice("La matrice non e\' invertibile.");
    }
    MatriceQuadrata mi = matriceIdentita();
    Matrice m = concatena(mi);
    Vector<Matrice> aux = m.eliminazioneGaussJordan();
    m = aux.get(1);

    MatriceQuadrata R = new MatriceQuadrata(getRighe());

    //copio i risultati in R
    int colonne = getColonne();
    for(int i=0; i < getRighe(); ++i) {
        for(int j=0; j < getRighe(); ++j) {
            R.setEl(m.getEL(i,j+colonne),i,j);
        }
    }
    return R;


	}

	public MatriceQuadrata trasponi() throws ErroriMatrice {
    int n = getRighe();
    double[][] aux= new double[n][n];

        for(int i = 0; i<n; ++i)
            for(int j = 0; j < n; ++j)
                aux[j][i] = getEL(i,j);

        return new MatriceQuadrata(aux,n);


	}

	public MatriceQuadrata eliminazioneGauss() {		    
	    Matrice a = super.eliminazioneGauss();

	  return new  MatriceQuadrata(a.getElementi(),a.getRighe());


	}

	public MatriceQuadrata add(MatriceQuadrata m) throws ErroriMatrice {

	    if((getRighe() != m.getRighe()) || (getColonne() != m.getColonne())) {
	        throw new ErroriMatrice("Le dimensioni delle due matrici non sono identiche.");
	    }
	    int n = getRighe();
	    double[][] aux= new double[n][getColonne()];

	    for(int i=0; i<n; ++i) {
	        for(int j=0; j<n; ++j) {
	             aux[i][j] = this.getEL(i,j) + m.getEL(i,j);
	        }
	    }
	    return new MatriceQuadrata(aux,n);
	}



	public MatriceQuadrata multiplyForScalar(double scalare) throws ErroriMatrice {
	    MatriceQuadrata risultato = new MatriceQuadrata(this);
	    int n = getRighe();
	    for(int i = 0; i < n; ++i)
	       for(int j = 0; j < n; ++j)
	           risultato.setEl(risultato.getEL(i,j) * scalare,i,j);
	    return risultato;

	}

	public MatriceQuadrata subtract(MatriceQuadrata m) throws ErroriMatrice {
	    
	    if((getRighe() != m.getRighe()) || (getColonne() != m.getColonne())) {
	        throw new ErroriMatrice("Le dimensioni delle due matrici non sono identiche.");
	    }
	    int n = getRighe();
	    double[][] aux= new double[n][getColonne()];

	    for(int i=0; i<n; ++i) {
	        for(int j=0; j<n; ++j) {
	             aux[i][j] = this.getEL(i,j) - m.getEL(i,j);
	        }
	    }
	    return new MatriceQuadrata(aux,n);


	}

}


