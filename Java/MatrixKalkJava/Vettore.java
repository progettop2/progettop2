package MatrixKalkJava;

import java.util.Vector;

public class Vettore extends Matrice {


	private boolean riga;

	private static Matrice checkDimensioni(double [][] ele,int dim, boolean c) throws ErroriMatrice {

    	if(ele.length >1 && c==true)
        	throw new ErroriMatrice("Errore nella creazione del vettore riga");

        if(ele.length==1 && c==false)
            throw new ErroriMatrice("Errore nella creazione del vettore colonna");
        
        if(c == true)
            return new Matrice(ele,1,dim);
        else
            return new Matrice(ele,dim,1);

	}

	private static Matrice checkDimensioni(int dimensione, double val, boolean r) throws ErroriMatrice{
        if(r == true)
            return new Matrice(1,dimensione,val);
        else
            return new Matrice(dimensione,1,val);

	}


	public Vettore(double[][] ele, int dimensione, boolean r) throws ErroriMatrice{
		super(checkDimensioni(ele,dimensione,r));
		riga = r;

	}

	public Vettore(int dimensione,double val,boolean r) throws ErroriMatrice{
		super(checkDimensioni(dimensione,val,r));
		riga=r;
	}

	public Vettore(Vettore m) {
		super(m.getElementi(),m.getRighe(),m.getColonne());
		riga=m.riga;

	}


	public boolean isColonna() {
        return !(riga);
	}

	public boolean isRiga() {
        return riga;
	}

	public int getDimensione() {
        if(riga)
            return getColonne();
        else
            return getRighe();
	}

	public void setEl(double val, int i) throws ErroriMatrice {

		if(riga)
			setEl(val,0,i);
		else
			setEl(val,i,0);
	}


	public double getEL(int i) throws ErroriMatrice {

		if(riga)
			return getEL(0,i);
		else
			return getEL(i,0);
	}

	public Vettore trasponi() throws ErroriMatrice {
		int col = getColonne();
    	int rig = getRighe();
        double[][] aux= new double[col][rig];

        int ele = rig * col;

        for(int i = 0; i<rig; ++i)
            for(int j = 0; j < col; ++j)
                aux[j][i] = getEL(i,j);

        return new Vettore(aux,ele,!(this.isRiga()));
	}

	public double norma() throws ErroriMatrice {

	   if(isRiga())
            throw new ErroriMatrice("La norma euclidea non e\' definita per vettori riga.");
    
        return Math.sqrt(prodottoScalare(this));

	}
 
	public Vettore normalizzazione() throws ErroriMatrice {
        if(isRiga())
            throw new ErroriMatrice("La normalizzazione non e\' definita per vettori riga.");
		 return (this).multiplyForScalar(1/norma());

	}


	public Vettore subtract(Vettore u) throws ErroriMatrice{
	   
        boolean isR = isRiga();
        int n = getDimensione();
        if((isR != u.isRiga())|| (n != u.getDimensione()))
            throw new ErroriMatrice("Dimensioni dei due vettori non compatibili.");

        int col = getColonne();
        int rig = getRighe();
        double[][] aux = new double[rig][col];
        if(isR) {
           for(int i = 0; i < n; ++i) {
               aux[0][i] = this.getEL(0,i) - u.getEL(0,i);
            }
        }
        else{
           for(int i = 0; i < n; ++i) {
               aux[i][0] = this.getEL(i,0) - u.getEL(i,0);
            }
 
        }
        return new Vettore(aux,n,isR);         
	}


	public Vettore add(Vettore u) throws ErroriMatrice {
	
    	boolean isR = isRiga();
        int n = getDimensione();
        if((isR != u.isRiga())|| (n != u.getDimensione()))
            throw new ErroriMatrice("Dimensioni dei due vettori non compatibili.");
        
        int col = getColonne();
        int rig = getRighe();
        double[][] aux = new double[rig][col];
        if(isR) {
            for(int i = 0; i < n; ++i) {
               aux[0][i] = this.getEL(0,i) + u.getEL(0,i);
            }
        }
        else{
            for(int i = 0; i < n; ++i) {
               aux[i][0] = this.getEL(i,0) + u.getEL(i,0);
            }
 
        }
        return new Vettore(aux,n,isR);      
    }

    public Vettore multiplyForScalar(double scalare) throws ErroriMatrice {
        Vettore risultato = new Vettore(this);
        if(isColonna()) {
            for(int i = 0; i < getRighe(); ++i)
               risultato.setEl(risultato.getEL(i,0)*scalare,i,0);//(*risultato)[i][0] *= scalare;
        }
        
        if(isRiga()) {
            for(int i = 0; i < getColonne(); ++i)           
               risultato.setEl(risultato.getEL(0,i)*scalare,0,i);    
        }      
        return risultato;
	}


	

	public double distanza(Vettore u) throws ErroriMatrice {
	
    	if((isColonna() != u.isColonna())|| (isRiga() != u.isRiga()) || (getDimensione()!=u.getDimensione()))
            throw new ErroriMatrice("Le dimensioni dei vettori non sono identiche.");
        

        Vettore aux = this.subtract(u);
        
        return aux.norma();

	}

	public Vettore eliminazioneGauss(){

		Matrice a = eliminazioneGauss();
    	return (Vettore)a;

	}

	public double angolo(Vettore v, boolean dec) throws ErroriMatrice {
    	if(dec)
            return ((Math.acos((prodottoScalare(v))/(norma()*v.norma())))*180)/Math.PI;

        return Math.acos((prodottoScalare(v))/(norma()*v.norma()));


	}

	public double prodottoScalare(Vettore y) throws ErroriMatrice {

        Matrice ts = trasponi();
        Matrice ris=ts.multiply(y);

        double prodotto=ris.getEL(0,0);

        return prodotto;

	}

}