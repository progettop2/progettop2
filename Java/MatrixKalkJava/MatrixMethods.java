package MatrixKalkJava;
import java.util.Vector;


public interface MatrixMethods{

//implementati
	public double[][] getElementi();

	public int getColonne();

    public int getRighe();

	public double[] getRiga(int i)throws ErroriMatrice;

    public double getEL(int i, int j)throws ErroriMatrice;

	public void setEl(double valore,int i,int j);

	public void stampa() throws ErroriMatrice;

	public MatrixMethods trasponi() throws ErroriMatrice;

	public void randomFill();

	public void clear();

	public MatrixMethods eliminazioneGauss();

	public MatrixMethods multiplyForScalar(double scalare) throws ErroriMatrice;

	public int rango();
		
}