package MatrixKalkJava;
import java.util.Vector;
import java.util.Random;

public class Matrice implements MatrixMethods {
private int nRighe = 1;
private int nColonne = 1;

private double[][] elementi;

	public Matrice(int r, int c) {

		nRighe=r;
		nColonne=c;

		elementi = new double[nRighe][nColonne];
	}

	public Matrice(double[][] a, int r, int c){

		nRighe=r;
		nColonne=c;
		elementi=a;

	}


	public Matrice(double[][] a,int r) {
		nColonne = 1;
		nRighe = r;
		elementi = a; 
	}

	public Matrice(int r, int c, double val) {
		elementi= new double[r][c];
		nRighe=r;
		nColonne=c;

		for(int i=0; i < nRighe; i++) {
			for(int j=0; j < nColonne; j++)
				elementi[i][j]=val;
		}

	}

	//costruttore di copia
	public Matrice(Matrice a) {
		
        nColonne=a.nColonne;
        nRighe=a.nRighe;
        
        int length = a.elementi.length;
        double[][] target = new double[length][a.elementi[0].length];
        for (int i = 0; i < length; i++) {
    
            System.arraycopy(a.elementi[i], 0, target[i], 0, a.elementi[i].length);
        }

        elementi=target;
	}



	public double[] getRiga(int i) throws ErroriMatrice {
		if(i >= nRighe)
			throw new ErroriMatrice("Out_of_Bounds");
	
        double[] riga = new double[elementi[i].length];

        System.arraycopy( elementi[i], 0, riga, 0, elementi[i].length );
    		
    	return riga;
    }

	public int getRighe() {
		return nRighe;
	}

	public double[][] getElementi() {
		return elementi;
	}

	public int getColonne() {
		return nColonne;
	}

	public double getEL(int i, int j) throws ErroriMatrice {
		if(i >= nRighe|| j>= nColonne || i < 0|| j < 0)
			throw new ErroriMatrice("Out of Bounds");


		return elementi[i][j];
	}

	public void setEl(double valore, int i, int j) {

		elementi[i][j]= valore;
	}

	public void stampa() throws ErroriMatrice {	
		String print[][] = new String[nRighe][nColonne];
		int max = 0;
	    for(int i = 0; i < nRighe; ++i) {
	        for(int j = 0; j < nColonne; ++j) {
				double number= Math.round(getEL(i,j)*1000.0)/1000.0;
				print[i][j] = Double.toString(number);
				if(print[i][j].length() > max)
					max = print[i][j].length();
			}
		}

		System.out.print("\n");
		for(int i = 0; i < nRighe; ++i) {
	        for(int j = 0; j < nColonne; ++j) {
	          if(print[i][j].length()< max) {
				  String space="";

				  int diff  = max - print[i][j].length();
				  for(int s = 0; s<diff; ++s) {
					space = space+" ";
				  }
				  print[i][j]+=space;
			  }
				
	          System.out.print(print[i][j]+"|");
	        }
	       System.out.println("");
	    }
	    System.out.print("\n");
	}

	public Matrice trasponi() throws ErroriMatrice{
		
        double[][] aux= new double[nColonne][nRighe];

    for(int i = 0; i<nRighe; ++i)
        for(int j = 0; j < nColonne; ++j)
            aux[j][i] = getEL(i,j);

        return new Matrice(aux,nColonne,nRighe);
	}


	public void randomFill() {
        Random generator = new Random();
		for(int i = 0; i<nRighe; ++i) {
	        for(int j = 0; j < nColonne; ++j) {
	          double k = (-50.0) + (generator.nextDouble()*100.0);
	          elementi[i][j]=k;
	        }
	    }

	}


	public void clear() {
		  
		for(int i = 0; i<nRighe; ++i) {  
	        for(int j = 0; j < nColonne; ++j) {
	      
	          elementi[i][j]=0;
	      
	        }
		}
	}

	public Matrice concatena(Matrice b) throws ErroriMatrice {
	   
	    if(nRighe != b.nRighe) {
	        throw new ErroriMatrice("Le matrici non sono concatenabili.");
	    }

	    Matrice ris = new Matrice(nRighe,nColonne+b.nColonne);
	   
	    for(int i=0; i < nRighe; ++i) {
	        for(int j=0; j < nColonne; ++j) {

	            ris.elementi[i][j]=elementi[i][j];
	        }
	    }
	   
	    for(int i=0; i < b.nRighe; ++i) {
	        for(int j=0; j < b.nColonne; ++j) {
	            ris.elementi[i][nColonne+j]=b.elementi[i][j];
	        }
	    }
	    return ris;

	}

	public Vector<Matrice> decomposizionePLU() {

        int dimL  = nRighe;

        Matrice P = new Matrice(dimL, dimL,0);
        Matrice  L = new Matrice(dimL, dimL,0);
        
        Matrice  U = new Matrice(this);

        double[] aux=new double[nColonne];


        int k = 0;

        for(int i = 0; i < dimL; ++i) {
            P.elementi[i][i] = 1.0;
            L.elementi[i][i] = 1.0;
        }

        for(int i = 0; i < nColonne && k < nRighe; ++i) {
            int maxRiga = k;
            double maxEl = Math.abs(U.elementi[k][i]);

            for(int j = i+1; j < nRighe; ++j) {
                if(Math.abs(U.elementi[j][i]) > maxEl) {
                    maxEl = Math.abs(U.elementi[j][i]);
                    maxRiga = j;
                }
            }
            if(maxEl != 0) {


                if(maxRiga != i) {
                    aux = U.elementi[i];
                    U.elementi[i] = U.elementi[maxRiga];
                    U.elementi[maxRiga] = aux;

                    aux = P.elementi[i];
                    P.elementi[i] = P.elementi[maxRiga];
                    P.elementi[maxRiga] = aux;
                }
                for(int j = k+1; j < nRighe; ++j) {
                     double moltiplicatore = -(U.elementi[j][i]/maxEl);

                     for(int s = 0; s < nColonne; ++s) {
                         if(s == i) {
                             U.elementi[j][s] = 0;
                             L.elementi[j][s] = moltiplicatore * (-1.0);
                         }
                         else {
                             U.elementi[j][s] += moltiplicatore*(U.elementi[k][s]);
                         }
                     }
                }
                k++;
            }
        }


        Vector<Matrice> PLU= new Vector <Matrice>();
        PLU.add(P);
        PLU.add(L);
        PLU.add(U);
        return PLU;
	}

	public Vector<Matrice> eliminazioneGaussJordan() {

    	Vector<Matrice> aux = this.decomposizionePLU();
        Vector<Matrice> GJ = new Vector<Matrice>();
        GJ.add(aux.get(0));
       	GJ.add(aux.get(2));


       	
        for(int i=(GJ.get(1).getRighe()-1); i>=0; --i) {
            boolean pivotTrovato = false;
            double divisore = 1.0;

            int w=GJ.get(1).getColonne();
            for(int j=0; j < w; ++j) {
                // ho trovato il pivot
                if((GJ.get(1)).elementi[i][j] != 0 && !pivotTrovato){
                   
                    divisore = (GJ.get(1)).elementi[i][j];
                    pivotTrovato = true;

                    int y=(GJ.get(1)).getColonne();             
                    for(int z=j; z < y; ++z) {
                        (GJ.get(1)).elementi[i][z] /= divisore;
                    }
                    for(int k=i-1; k>=0; --k) {
                        double sott = (GJ.get(1)).elementi[k][j];
                        
                        for(int s = j; s < (GJ.get(1)).getColonne(); ++s) {
                                ((GJ.get(1))).elementi[k][s] -= sott*(((GJ.get(1))).elementi[i][s]);
                        }
                    }
                } 
            }
        }
        return GJ;
	}	
	

	public Matrice eliminazioneGauss() {
		return	(this.decomposizionePLU().get(2));

	}


	final public Matrice multiply(Matrice m) throws ErroriMatrice{
        if(nColonne != m.nRighe) {
            throw new ErroriMatrice("Le colonne della prima matrice non sono uguali alle righe della seconda matrice");
        }
        //creo una matrice sullo heap nulla di dimensioni n*p
        double[][] risultato= new double[nRighe][m.nColonne];

        for(int i = 0; i < nRighe; ++i) {
            for(int j = 0; j < m.nColonne; ++j) {
                double aux = 0;
                for(int k = 0; k < m.nRighe; ++k) {
                    aux = aux + this.elementi[i][k] * m.elementi[k][j];
                }
                risultato[i][j] = aux;
            }
        }
        return  new Matrice(risultato,nRighe,m.nColonne);

	}

	public Matrice add(Matrice m) throws ErroriMatrice {
        if((nRighe != m.nRighe) || (nColonne != m.nColonne)) {
            throw new ErroriMatrice("Le dimensioni delle due matrici non sono identiche");
        }

        double[][] aux = new double[nRighe][nColonne];
        
        for(int i=0; i<nRighe; ++i) {
            for(int j=0; j<nColonne; ++j) {
                 aux[i][j] = this.elementi[i][j] + m.elementi[i][j];
            }
        }
        return new Matrice(aux,nRighe,nColonne);


    	}


    	public Matrice subtract(Matrice m) throws ErroriMatrice {
        if((nRighe != m.nRighe) || (nColonne != m.nColonne)) {
            throw new ErroriMatrice("Le dimensioni delle due matrici non sono identiche");
        }

        double[][] aux = new double[nRighe][nColonne];
        
        for(int i=0; i<nRighe; ++i) {
            for(int j=0; j<nColonne; ++j) {
                 aux[i][j] = this.elementi[i][j] - m.elementi[i][j];
            }
        }
        return new Matrice(aux,nRighe,nColonne);

    	}


    	public Matrice multiplyForScalar(double scalare) throws ErroriMatrice {

    	Matrice risultato =  new Matrice(this);
        for(int i = 0; i < nRighe; ++i)
           for(int j = 0; j < nColonne; ++j) {
            risultato.setEl(risultato.getEL(i,j) * scalare,i,j);
        }
        return risultato;


	}

	public int rango() {
    	Vector<Matrice> aux = decomposizionePLU();
        Matrice gauss = aux.get(2);
        int ris = 0;
        boolean pivotTrovato = false;
        for(int i = 0; i < nRighe; ++i) {
            pivotTrovato = false;
            for(int j = 0; j < nColonne && !pivotTrovato; ++j) {
                if(gauss.elementi[i][j] != 0) {
                    ris++;
                    pivotTrovato = true;
                }
            }
        }
        return ris;
	}

	public boolean Equals(Matrice m) {
      	if(nRighe!=m.nRighe||nColonne!=m.nColonne) return false;
        for(int i = 0; i < nRighe; ++i) {
            for(int j = 0; j < nColonne; ++j) {
                if(elementi[i][j]!=m.elementi[i][j])
                return false;
            }
        }
    	return true;
	}

}
