package MatrixKalkJava;
import java.text.DecimalFormat;

public class Complesso{

	private double reale;
	private double img;

	public Complesso(){
		reale=0.0;
		img=0.0;
	}

	
	public Complesso(double r){
		reale=r;
	}

	public Complesso(double r,double i) {

		reale=r;
		img= i;
	}

	public boolean isNull(){

		if(reale==0.0 && img==0.0)
			return true;

	return false;

	}

	public Complesso add(Complesso b) {

		Complesso a= new Complesso((reale+b.reale),(img+b.img));
		return a;
	}

	public Complesso subtract(Complesso b) {

		Complesso a= new Complesso((reale-b.reale),(img-b.img));
		return a;

	}

	public Complesso multiply(Complesso b) throws ErroriComplesso{
		if(isNull() && b.isNull())
        	throw new ErroriComplesso("Numero non Definito");


		double pReale=(reale*b.reale)-(img*b.img);
		double pImg=(reale*b.img)+(img*b.reale);

		Complesso a= new Complesso(pReale,pImg);
		return a;

	}

	public Complesso divide(Complesso b) throws ErroriComplesso {
		double pReale = (reale*b.reale)+(img*b.img);
		double pImg = (img*b.reale)-(reale*b.img);

	 
	    if(b.isNull())
        throw new ErroriComplesso("Divisione Per Zero");
    	

    	double denominatore = (reale*b.reale)+(img*b.img);
    
    	Complesso a = new Complesso((pReale/denominatore),(pImg/denominatore));
    	return a;
	}

	public void setParteReale(double r) {
		reale=r;

	}

	public void setParteImmaginaria(double i) {
		img=i;

	} 

	public double getParteReale() {
		return reale;

	}

	public double getParteImg() {
		return img;
	}

	public Complesso coniugato(){

		Complesso a = new Complesso(reale,img*(-1.0));
		return a;
	}

	public double modulo() {
		double ris= Math.sqrt((reale*reale)+(img*img));

		return ris;
	}


	public double argomento()throws ErroriComplesso {
    if(reale == 0 && img == 0)
        throw new ErroriComplesso("Numero non Definito");


		double PI = Math.atan(1.0)*4;

			 if(reale != 0) {
	        if(img<0)
	            return -PI/2.0;
	        return PI/2.0;
	    }
	  	double arg = Math.atan(img/reale);
	    
	    if(reale<0) {
	        if(img<0)
	            return arg-PI;
	        return arg+PI;
	    }
	    return arg;

	}


	public Complesso pow(int n) throws ErroriComplesso{
	if(n == 0)
        throw new ErroriComplesso("Impossibile elevare a potenza nulla");
		
		double r = this.modulo();
	    double theta = this.argomento();
	    theta = theta*n;
	    
	    r = Math.pow(r,n);
	    double sin = Math.sin(theta);
	    double cos = Math.cos(theta);

	    Complesso a= new Complesso(r*cos,r*sin);
	    return a;
	}

	public Complesso[] root(int n) throws ErroriComplesso {
	if(n==1 || n == 0)
        throw new ErroriComplesso("Impossibile calcolare la radice 'prima' o 'nulla'");

		double r= Math.pow(this.modulo(),(1.0/n));

		double theta;
   		double PI = Math.atan(1.0)*4;


		int k= Math.abs(n);
		Complesso[] radici = new Complesso[k];


	    for(int i=0; i<k; ++i) {
	        theta = (this.argomento()+(2*PI*i))/n;
	         Complesso j= new Complesso((r*(Math.cos(theta))),(r*(Math.sin(theta))));
	        radici[i]=j;
	    }

	    return radici;
	} 


	public void stampa() {
	DecimalFormat df= new DecimalFormat("#.##");
	char a ='+';
    if(img <= 0){
        a = ' ';
    }
    System.out.println(df.format(reale) + " "+a+df.format(img)+"i");

	}


}