package MatrixKalkJava;

public class ErroriComplesso extends Exception{

	private String err="si e\' verificato un errore non previsto";

	public ErroriComplesso(String text) {
        super(text);
    }

    public ErroriComplesso(String text, Throwable exc) {
        super(text, exc);
    }

    public ErroriComplesso(Throwable exc) {
    	super(exc);
    }

        public String what(){
    	return err;

    }
}