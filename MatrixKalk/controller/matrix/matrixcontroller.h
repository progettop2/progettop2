#ifndef MATRIXCONTROLLER_H
#define MATRIXCONTROLLER_H

#include "../../model/matrice.h"
#include "../../model/matricequadrata.h"
#include "../../model/vettore.h"
#include "../../model/eccezioni.h"
#include "../../view/matrix/abstractmatrixgui.h"
#include <typeinfo>
#include <vector>
#include <iostream>
#include <QLineEdit>
#include <QString>
#include <QDebug>
using std::vector;
class MatrixController
{
public:
    MatrixController(abstractMatrixGUI*);

    ~MatrixController();


    void memoryClear();
    void clearResults();
    void clearOperandSx();
    void clearOperandDx();
    void clearAllOperands();
    vector<vector<double>> truncate(vector<vector<double>>,unsigned int dec = 3) const;
    void buildOperand(vector< vector<QLineEdit*> >, bool);
    void switchM();

    //Operazioni
    void add();//fatto
    void subtract();
    void rank(bool);//fatto
    void traspose(bool);//fatto
    //operazioni problematiche
    void multiply();//fatto
    void scalarMultiply(double,bool);//fatto
    void PLUdecomp(bool);//fatto
    void GaussElimination(bool);
    void GaussJordanElimination(bool);
    //Matrice Quadrata
    void determinant(bool);//fatto
    void inverse(bool);//fatto
    //Vettore
    void distance();
    void norm(bool);
    void angle();
    void normalization(bool);
    //Metodi propriamente del controller
    void assignResultToOp(bool,unsigned int);//fatto


private:
    static bool isSquare(Matrice*);
    static bool isVector(Matrice*);
    static Vettore* toVector(Matrice*);
    static MatriceQuadrata* toSquare(Matrice *);
    //Puntatori a matrice
    Matrice *sxM, *dxM;
    vector<Matrice*> results;
    abstractMatrixGUI* view;
    MatrixController(const MatrixController&);
    MatrixController& operator=(const MatrixController&);
};


#endif // MATRIXCONTROLLER_H

