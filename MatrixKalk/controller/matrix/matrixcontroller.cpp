#include "matrixcontroller.h"
#include <string>
using std::vector;


MatrixController::MatrixController(abstractMatrixGUI * v) {
    view = v;
    sxM = new MatriceQuadrata(5);
    dxM = new MatriceQuadrata(5);
    results.resize(3);
    for(int i = 0; i < 3; ++i)
        (results[i]) = new Matrice(5,5);
}

MatrixController::~MatrixController() {
    clearAllOperands();
    clearResults();
}

//La costruzione di copia di un controller non fa memoria condivisa per
//i puntatori al model ma fa memoria condivisa con la view
//siccome non si possono fare construzioni di copia di QWidget



vector<vector<double>> MatrixController::truncate(vector< vector<double> > el, unsigned int decimalToKeep) const {
    vector<vector<double> > ris(el.size(), vector<double>(el[0].size()));
    for(unsigned int i = 0; i<el.size(); ++i) {
        for(unsigned int j=0; j<el[0].size(); ++j) {
           ris[i][j] = std::round(el[i][j] * pow(10,decimalToKeep)) / pow(10,decimalToKeep);
        }
    }
    return ris;
}

void MatrixController::clearOperandSx() {
    if(sxM)
        delete sxM;
}

void MatrixController::clearOperandDx() {
    if(dxM)
        delete dxM;
}

void MatrixController::clearAllOperands() {
    clearOperandSx();
    clearOperandDx();
}


void MatrixController::buildOperand(vector<vector<QLineEdit*> > elview,  bool sx) {
    int righe = elview.size();
    int colonne = elview[0].size();
    vector< vector< double > > elementi(righe, vector<double>(colonne));

    for(unsigned int i=0;i < elview.size(); i++) {
        for(unsigned int j=0; j< elview[0].size(); j++) {
            elementi[i][j]=elview[i][j]->text().toDouble();
        }
    }
    //ogni costruzione di matrice potrebbe generare un'eccezione
    //ogni eccezione è dello stesso tipo
    try {
        if(sx) {
            clearOperandSx();
            if(righe == colonne) {
                sxM = new MatriceQuadrata(elementi,righe);
                return;
            }
            if((righe == 1 && colonne > 1)) {
                sxM = new Vettore(elementi,colonne);
                return;
            }
            if((righe > 1 && colonne == 1)) {
                sxM = new Vettore(elementi,righe,false);
                return;
            }
            sxM = new Matrice(elementi,righe,colonne);
        }
        else {
            clearOperandDx();
            if(righe == colonne) {
                dxM = new MatriceQuadrata(elementi,righe);               
                return;
            }
            if((righe == 1 && colonne > 1)) {
                dxM = new Vettore(elementi,colonne);
                return;
            }
            if((righe > 1 && colonne == 1)) {
                dxM = new Vettore(elementi,righe,false);
                return;
            }
            dxM = new Matrice(elementi,righe,colonne);
        }

    }
    catch(matrice_error& ex) {
        //costruisco una dialog di errore
        view->displayMessaggioErrore(ex.what());
    }
}

void MatrixController::memoryClear() {
    clearAllOperands();
    clearResults();
}

void MatrixController::clearResults() {
    for (std::vector< Matrice* >::iterator it = results.begin() ; it != results.end(); ++it)
      {
        delete (*it);

      }
      results.clear();
}


bool MatrixController::isSquare(Matrice* a) {
    return a->getColonne() == a->getRighe();

}


bool MatrixController::isVector(Matrice* a) {
    return ((a->getColonne() == 1 && a->getRighe() > 1) || (a->getColonne() > 1 && a->getRighe() == 1));
}


MatriceQuadrata* MatrixController::toSquare(Matrice* a) {
    if(isSquare(a)) {
       return new MatriceQuadrata(a->getElementi(),a->getColonne());
    }
    else
        throw controller_error("non è quadrata.");
}


Vettore* MatrixController::toVector(Matrice* a) {
    if(isVector(a)) {
        if(a->getColonne()>1){
            return new Vettore(a->getElementi(),a->getColonne());
        }
        else
            return new Vettore(a->getElementi(),a->getRighe(),false);

    }
    throw controller_error("non è un vettore.");

}
//---------------------------
// metodi matematici
//---------------------------

void MatrixController::multiply() {

    Matrice* resultAux;
    try {
        if(view->matrixViewModified(true)) {
            buildOperand(view->getMatrixContent(true),true);
        }
        if(view->matrixViewModified(false)){
            buildOperand(view->getMatrixContent(false),false);
        }
         resultAux = (*sxM)*(*dxM);
         if(isSquare(resultAux)){
             clearResults();
             MatriceQuadrata* auxSQ = new MatriceQuadrata(resultAux->getElementi(),resultAux->getColonne());
             results.push_back(auxSQ);
             delete resultAux;
         }
         else if(isVector(resultAux)){
             clearResults();
             Vettore* auxV;
             if(resultAux->getColonne()>1)
                 auxV = new Vettore(resultAux->getElementi(),resultAux->getColonne());
             else
                 auxV = new Vettore(resultAux->getElementi(),resultAux->getRighe(),false);

             results.push_back(auxV);
             delete resultAux;
         }
         else {
            clearResults();
            results.push_back(resultAux);
         }
    }
    catch(matrice_error& e){
        view->displayMessaggioErrore(e.what());
        return;
    }
    view->setTitoloRisultato("A * B:");
    view->stampaRis(results[0]->getElementi(),posRisultato::left);
}

void MatrixController::add() {
    try {
        if(view->matrixViewModified(true)) {
            buildOperand(view->getMatrixContent(true),true);
        }
        if(view->matrixViewModified(false)){
            buildOperand(view->getMatrixContent(false),false);
        }

        Matrice* aux;       
        aux = sxM->operator+(*dxM);
        clearResults();
        results.push_back(aux);

        view->setTitoloRisultato("A + B:");
        view->stampaRis(results[0]->getElementi(),posRisultato::left);
    }

    catch(matrice_error& e) {
        view->displayMessaggioErrore(e.what());
        return;
    }
    catch(controller_error& e){
        view->displayMessaggioErrore(QString("B ")+QString(e.what()));
        return;
    }
}

//la moltiplicazione per scalare non puo' lanciare eccezioni in questo contesto
//nell' operator*(const double) ridefinito costrusce di copia una nuova matrice a partire dalla matrice d'invocazione
//siccome la matrice d'invocazione esiste (è stata costruita con successo) allora anche la matrice ausiliria non solleverà
//eccezioni
void MatrixController::scalarMultiply(double scalar, bool operand) {
    //operand == true, devo moltiplicare per l'operatore sx
    if(view->matrixViewModified(operand)) {
        buildOperand(view->getMatrixContent(operand),operand);
    }
    clearResults();
    if(operand) {
        results.push_back(sxM->operator*(scalar));
        view->setTitoloRisultato("A * "+QString::number(scalar)+" :");
    }
    else{
       results.push_back(dxM->operator*(scalar));
       view->setTitoloRisultato("B * "+QString::number(scalar)+" :");
    }

    view->stampaRis((results[0])->getElementi(),posRisultato::left);
}


//Come per scalarMultiply, determinant non puo' lanciare eccezioni in questo contesto
void MatrixController::determinant(bool operand) {
    if(view->matrixViewModified(operand)) {
        buildOperand(view->getMatrixContent(operand),operand);
    }
    if(operand) {
        if(dynamic_cast<MatriceQuadrata*>(sxM)) {
            view->stampaRisNumerico((static_cast<MatriceQuadrata*>(sxM))->determinante(),"Determintante di A:");
            return;
        }
        else {
            throw controller_error("Impossibile calcolare il determinante: A non è una matrice quadrata.");
        }
    }
    if(dynamic_cast<MatriceQuadrata*>(dxM)) {
        view->stampaRisNumerico((static_cast<MatriceQuadrata*>(dxM))->determinante(),"Determintante di B:");
    }
    else {
        throw controller_error("Impossibile calcolare il determinante: B non è una matrice quadrata.");
    }
}

void MatrixController::inverse(bool operand) {
     try{
         if(view->matrixViewModified(operand)) {
             buildOperand(view->getMatrixContent(operand),operand);
         }
         if(operand) {
             if(dynamic_cast<MatriceQuadrata*>(sxM)) {
                 clearResults();
                 results.push_back(new MatriceQuadrata((toSquare(sxM))->inversa()));
                 view->setTitoloRisultato("Inversa di A: ");
             }
             else {
                 throw controller_error("Non è possibile trovare l'inversa: A non è quadrata.");
             }
         }
         else {
             if(dynamic_cast<MatriceQuadrata*>(dxM)) {
                 clearResults();
                 results.push_back(new MatriceQuadrata((toSquare(dxM))->inversa()));
                 view->setTitoloRisultato("Inversa di B: ");
             }
             else {
                 throw controller_error("Non è possibile trovare l'inversa: A non è quadrata.");
             }
         }
         view->stampaRis((results[0])->getElementi(),posRisultato::left);
     }
     catch(matrice_error& e) {
         view->displayMessaggioErrore(QString::fromStdString(e.what()));
     }
}

void MatrixController::rank(bool operand) {
    if(view->matrixViewModified(operand)) {
        buildOperand(view->getMatrixContent(operand),operand);
    }
    if(operand) {
        view->stampaRisNumerico(sxM->rango(),"Rango di A:");
    }
    else{
        view->stampaRisNumerico(dxM->rango(),"Rango di B:");
    }

}

void MatrixController::PLUdecomp(bool operand) {
    if(view->matrixViewModified(operand)) {
        buildOperand(view->getMatrixContent(operand),operand);
    }
    clearResults();
    if(operand) {
        results = sxM->decomposizionePLU();
        view->setTitoloRisultato("Decomposizione PLU di A: ");
    }
    else {
        results = dxM->decomposizionePLU();
        view->setTitoloRisultato("Decomposizione PLU di B: ");
    }
    for(unsigned int i = 0; i<results.size(); ++i) {
        if(isSquare(results[i])) {
            results[i] = toSquare(results[i]);
        }
        else if(isVector(results[i]))
            results[i] = toVector(results[i]);
    }
    view->stampaRis((results[0])->getElementi(),posRisultato::left,"P");
    view->stampaRis((results[1])->getElementi(),posRisultato::center,"L");
    view->stampaRis((results[2])->getElementi(),posRisultato::right,"U");
}

//trasposizione di una matrice, si utilizza il puntatore ausiliario in quanto la trasposizione potrebbe lanciare
//eccezioni in questo modo viene prima eseguita trasponi() che potrebbe lanciare eccezioni e poi si puliscono i risultati
//dal vector di puntatori a matrice, in questo modo non c'è il rischio di pulire prima i risultati e poi chiamare una funzione
//che potrebbe lanciare eccezioni, le quali se venissiro lanciate in questo contesto non permetterebbero il recupero dall'errore.
void MatrixController::traspose(bool operand) {
   try{
        if(view->matrixViewModified(operand)) {
            buildOperand(view->getMatrixContent(operand),operand);
        }
        Matrice* aux;
        if(operand) {
            aux = sxM->trasponi();
            clearResults();
            results.push_back(aux);
            view->setTitoloRisultato("Trasposta di A: ");
        }
        else {
            aux = dxM->trasponi();
            clearResults();
            results.push_back(aux);
            view->setTitoloRisultato("Trasposta di B: ");
        }
    }
    catch(matrice_error& ex) {
        view->displayMessaggioErrore(ex.what());
        return;
    }
    view->stampaRis(results[0]->getElementi(),posRisultato::left);
}

void MatrixController::assignResultToOp(bool operand,unsigned int i) {
    if(i > results.size())
        throw controller_error("Errore durante l'assegnazione del risultato");

    MatriceQuadrata* auxSQ = dynamic_cast<MatriceQuadrata*>(results[i]);
    Vettore* auxV = dynamic_cast<Vettore*>(results[i]);
    if(operand) {
        clearOperandSx();
        //faccio una copia non condivisa
        if(auxSQ){
            sxM = new MatriceQuadrata(*auxSQ);
            return;
        }
        else if(auxV){
            sxM = new Vettore(*auxV);
            return;
        }
        sxM = new Matrice(*(results[i]));
        return;
    }
    clearOperandDx();
    if(auxSQ){
        dxM = new MatriceQuadrata(*auxSQ);
        return;
    }
    else if(auxV){
        dxM = new Vettore(*auxV);
        return;
    }
    dxM = new Matrice(*(results[i]));
}

void MatrixController::subtract() {
    try {
        if(view->matrixViewModified(true)) {
            buildOperand(view->getMatrixContent(true),true);
        }
        if(view->matrixViewModified(false)){
            buildOperand(view->getMatrixContent(false),false);
        }

        Matrice* aux;

            aux = sxM->operator-(*dxM);
            clearResults();
            results.push_back(aux);

        view->setTitoloRisultato("A - B :");
        view->stampaRis(results[0]->getElementi(),posRisultato::left);
    }
    catch(matrice_error& e) {
        view->displayMessaggioErrore(e.what());
        return;
    }
    catch(controller_error& e){
        view->displayMessaggioErrore(QString("B ")+QString(e.what()));
        return;
    }
}

void MatrixController::GaussElimination(bool operand) {
    if(view->matrixViewModified(operand)) {
        buildOperand(view->getMatrixContent(operand),operand);
    }
    clearResults();
    if(operand){
        results.push_back(sxM->eliminazioneGauss());
        view->setTitoloRisultato("Eliminazione di Gauss su A: ");
    }
    else {
        results.push_back(dxM->eliminazioneGauss());
        view->setTitoloRisultato("Eliminazione di Gauss su B: ");
    }
    view->stampaRis((results[0])->getElementi(),posRisultato::left);
}

void MatrixController::GaussJordanElimination(bool operand) {
    if(view->matrixViewModified(operand)) {
        buildOperand(view->getMatrixContent(operand),operand);
    }
    clearResults();
    if(operand) {
        results = sxM->eliminazioneGaussJordan();
        view->setTitoloRisultato("Eliminazione di Gauss-Jordan su A: ");
    }
    else {
        results = dxM->eliminazioneGaussJordan();
        view->setTitoloRisultato("Eliminazione di Gauss-Jordan su B: ");
    }
    for(unsigned int i = 0; i<results.size(); ++i) {
        if(isSquare(results[i]))
            results[i] = toSquare(results[i]);
        else if(isVector(results[i]))
            results[i] = toVector(results[i]);
    }
    view->stampaRis((results[0])->getElementi(),posRisultato::left,"P");
    view->stampaRis((results[1])->getElementi(),posRisultato::center);
}

void MatrixController::switchM() {
    Matrice *aux = new Matrice(*sxM);
    delete sxM;
    sxM = dxM;
    dxM = aux;

    if(isSquare(aux)) {
       dxM = new MatriceQuadrata(*(static_cast<MatriceQuadrata*>(aux)));
       delete aux;
    }
    if(isVector(aux)) {
       bool c = true;
       int n = (aux->getRighe())*(aux->getColonne());

       if(aux->getRighe()>1)
           c = false;

       dxM = new Vettore(aux->getElementi(),n,c);
       delete aux;
    }
}

void MatrixController::norm(bool operand) {
    if(view->matrixViewModified(operand)) {
        buildOperand(view->getMatrixContent(operand),operand);
    }
    try{
        if(operand) {
            if(dynamic_cast<Vettore*>(sxM)) {
                view->stampaRisNumerico(static_cast<Vettore*>(sxM)->norma(),"Norma di A:");
            }
            else {
                throw controller_error("A non è un vettore.");
            }
        }
        else{
            if(dynamic_cast<Vettore*>(dxM)) {
                view->stampaRisNumerico(static_cast<Vettore*>(dxM)->norma(),"Norma di B:");
            }
            else {
                throw controller_error("B non è un vettore.");
            }
        }
    }
    catch(matrice_error& e) {
        view->displayMessaggioErrore(QString(e.what()));
    }
}

void MatrixController::distance() {
    if(view->matrixViewModified(true)) {
        buildOperand(view->getMatrixContent(true),true);
    }
    if(dynamic_cast<Vettore*>(sxM)) {
        try {
            view->stampaRisNumerico(static_cast<Vettore*>(sxM)->distanza(*(toVector(dxM))),"Distanza tra A e B:");
        }
        catch(controller_error& e) {
            view->displayMessaggioErrore(QString("B ")+QString(e.what()));
        }
        catch(matrice_error& e) {
            view->displayMessaggioErrore(e.what());
        }
    }
    else{
         throw controller_error("A non è un vettore");
    }
}

void MatrixController::angle() {
    if(view->matrixViewModified(true)) {
        buildOperand(view->getMatrixContent(true),true);
    }
    if(view->matrixViewModified(false)){
        buildOperand(view->getMatrixContent(false),false);
    }
    try {
        if(dynamic_cast<Vettore*>(sxM)){
           double dec =(static_cast<Vettore*>(sxM))->angolo(*(toVector(dxM)),true);
           double rad = (static_cast<Vettore*>(sxM))->angolo(*(toVector(dxM)),false);

            QString messaggio = "Angolo tra A e B: \n"+QString::number(dec)+"° \n"+QString::number(rad)+" rad";
            view->stampaRisNumerici(messaggio);
        }
        else
            throw controller_error("A non è un vettore.");
    }
    catch(controller_error& e){
        view->displayMessaggioErrore("B "+QString(e.what()));
    }
    catch(matrice_error& e){
        view->displayMessaggioErrore(QString(e.what()));
    }
}

void MatrixController::normalization(bool operand) {
    if(view->matrixViewModified(operand)) {
        buildOperand(view->getMatrixContent(operand),operand);
    }
    try{
        if(operand) {
            if(dynamic_cast<Vettore*>(sxM)) {
                Vettore* aux= (static_cast<Vettore*>(sxM))->normalizzazione();
                clearResults();
                results.push_back(aux);
                view->setTitoloRisultato("Normalizzazione di A: ");
                view->stampaRis((results[0])->getElementi(),posRisultato::left);
            }
            else{
                throw controller_error("A non è un vettore");
            }
        }
        else{
            if(dynamic_cast<Vettore*>(dxM)) {
                Vettore* aux= (static_cast<Vettore*>(dxM))->normalizzazione();
                clearResults();
                results.push_back(aux);
                view->setTitoloRisultato("Normalizzazione di B: ");
                view->stampaRis((results[0])->getElementi(),posRisultato::left);
            }
            else{
                throw controller_error("A non è un vettore");
            }
        }
    }
    catch(matrice_error& e){
        view->displayMessaggioErrore(QString(e.what()));
    }
}
