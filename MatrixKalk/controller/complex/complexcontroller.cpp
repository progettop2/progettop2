#include "complexcontroller.h"

ComplexController::ComplexController(abstractComplexGui* v):primo(new Complesso),aux(new Complesso),view(v),operazioneRichiesta(0){}


ComplexController::~ComplexController() {}

/**
 * @brief ComplexController::inserisciNumero permette di inserire un numero tramite il tastierino numerico
 * @param bottone contiene una copia del puntatore del chiamante, serve a determinare quale numero inserire
 */
void ComplexController::inserisciNumero(QPushButton* bottone)  {

    if(appenaEseguito) {
        view->setTextR("");
        view->setTextI("");
        appenaEseguito = false;
    }

    if(view->getDisplayUsato()==ComplexT::reale){

        if(bottone->text()==".") {

            view->setTextR(view->getTextR() + ".");
            return;
        }

        if(bottone->text()=="0"&& view->getTextR().length()==0) {
            return;
        }

        int x = bottone->text().toInt();
        view->setTextR(view->getTextR() + QString::number(x));
        return;

    }else{

            if(bottone->text()==".") {
                view->setTextI(view->getTextI() + ".");
                return;
            }

            if(bottone->text()=="0"&& view->getTextI().length()==0) {
                return;
            }

            int x = bottone->text().toInt();
            view->setTextI(view->getTextI() + QString::number(x));

            return;

    }

}

/**
 * @brief ComplexController::del cancella l'ultima cifra a destra del display selezionato
 */
void ComplexController::del() const {

    if(view->getDisplayUsato()==ComplexT::reale){
        QString text(view->getTextR());
        text.chop(1);
        view->setTextR(text);

    }else{
        QString text(view->getTextI());
        text.chop(1);
        view->setTextI(text);
    }
}

/**
 * @brief ComplexController::ac pulisce i due display e la cronologia delle operazioni effettuate
 */
void ComplexController::ac() const {

    view->clear();
    primo->setParteReale(0.0);
    primo->setParteImmaginaria(0.0);

}

/**
 * @brief ComplexController::setOperazioneRichiesta imposta l'operazione da eseguire una volta premuto il tasto =
 * @param a determina l'operazione richiesta
 */
void ComplexController::setOperazioneRichiesta(unsigned int a=0) {
    operazioneRichiesta=a;
    appenaEseguito = true;

}


/**
 * @brief ComplexController::esegui esegue l'operazione richiesta
 */
void ComplexController::esegui() {
    try{
        switch(operazioneRichiesta){

        case 4://potenza

            if(std::fmod(primo->getParteReale(),1)!=0) {
                view->enableDisplay(true);
                throw complesso_error("impossibile elevare a potenza frazionaria");
            }

            *primo=aux->pow(primo->getParteReale());

            view->setTextR(QString::number(primo->getParteReale()));
            view->setTextI(QString::number(primo->getParteImg()));
            view->enableDisplay(true);


            break;

        case 5://moltiplicazione
            *primo=(*aux)*(*primo);

            view->setTextR(QString::number(primo->getParteReale()));
            view->setTextI(QString::number(primo->getParteImg()));
            break;

        case 6://addizione
            *primo=(*aux)+(*primo);
            //update del display
            view->setTextR(QString::number(primo->getParteReale()));
            view->setTextI(QString::number(primo->getParteImg()));
            break;

        case 7://divisione
            *primo=(*aux) / (*primo);
            //update del display
            view->setTextR(QString::number(primo->getParteReale()));
            view->setTextI(QString::number(primo->getParteImg()));
            break;



        case 8://sottrazione

            *primo=(*aux)-*(primo);
            //update del display
            view->setTextR(QString::number(primo->getParteReale()));
            view->setTextI(QString::number(primo->getParteImg()));            
            break;


        }
        view->getExecuteButton()->setDisabled(true);
        setOperazioneRichiesta(0);
        appenaEseguito=true;
        aux->setParteReale(0.0);
        aux->setParteImmaginaria(0.0);
    }

    catch(complesso_error e) {

        view->displayMessaggioErrore(QString::fromStdString(e.what()));
        view->clear();
        view->enableDisplay(true);
    }


}
/**
 * @brief ComplexController::preparaOperazione prepara l'operazione per l'esecuzione
 * @param check controlla che l'operazione precedente sia stata eseguita, in caso contrario, evita di effettuare
 * nuovamente le assegnazioni
 */
void ComplexController::preparaOperazione(unsigned int check) {

    aux->setParteReale(primo->getParteReale());
    aux->setParteImmaginaria(primo->getParteImg());
    if(check !=4)
        view->enableDisplay(true);

    if(check>=4 &&check <=8)
        view->getExecuteButton()->setEnabled(true);

return;
}
/**
 * @brief ComplexController::updateComplesso aggiornai valori del complesso secondo il testo del display
 * @param display indica il display usato
 * @param text stringa da assegnare
 */
void ComplexController::updateComplesso(ComplexT display,const QString& text) const{
    if(display==ComplexT::reale){
        primo->setParteReale(text.toDouble());
    }
    if (display==ComplexT::immaginaria) {
        primo->setParteImmaginaria(text.toDouble());
    }
}

/**
 * @brief ComplexController::root elabora la funzione root in modo che possa essere letta dalla GUI
 * @param n indica il numero di radici desiderato
 * @return
 */
QString ComplexController::root(int n){

    QString text("");
    try {

        std::vector<Complesso> ris=primo->root(n);

        for(unsigned int i=0; i < ris.size() ;i++) {

            text.append(QString::number(ris[i].getParteReale()));
            text.append("+");
            text.append(QString::number(ris[i].getParteImg()));
            text.append("i\n");

        }

    }

    catch(complesso_error e) {

        view->displayMessaggioErrore(QString::fromStdString(e.what()));
        view->clear();
    }
return text;
}

int ComplexController::getOperazioneRichiesta() const {
    return operazioneRichiesta;
}

bool ComplexController::getAppenaEseguito() const {

    return appenaEseguito;
}

void ComplexController::setAppenaEseguito(bool a)  {
    appenaEseguito = a;
}


void ComplexController::argomento(){
try {
    aux->setParteReale(aux->argomento());
    aux->setParteImmaginaria(0.0);

    view->setTextR(QString::number(aux->getParteReale()));
    view->setTextI(QString::number(aux->getParteImg()));

    setOperazioneRichiesta(0);
    appenaEseguito=true;
    aux->setParteReale(0.0);
    aux->setParteImmaginaria(0.0);
    }
    catch(complesso_error e) {

        view->displayMessaggioErrore(QString::fromStdString(e.what()));
        view->clear();
    }
}


void ComplexController::modulo(){
try{
    view->setTextR(QString::number(aux->modulo()));
    view->setTextI(QString::number(0.0));

    setOperazioneRichiesta(0);
    appenaEseguito=true;
    aux->setParteReale(0.0);
    aux->setParteImmaginaria(0.0);

    }
    catch(complesso_error e) {

        view->displayMessaggioErrore(QString::fromStdString(e.what()));
        view->clear();
    }
}


void ComplexController::coniugato(){
    try{
        *aux=aux->coniugato();        
        view->setTextR(QString::number(aux->getParteReale()));
        view->setTextI(QString::number(aux->getParteImg()));

        setOperazioneRichiesta(0);
        appenaEseguito=true;
        aux->setParteReale(0.0);
        aux->setParteImmaginaria(0.0);
    }
    catch(complesso_error e) {

        view->displayMessaggioErrore(QString::fromStdString(e.what()));
        view->clear();
    }
}

QString ComplexController::restituisciRisultato(QString R, QString I) const{
    QString risultato=R;

    if(I=="0"||I=="")
        return risultato;

    if(I.toDouble()< 0)
        risultato.append(I+"i");
    else
        risultato.append("+"+I+"i");

    return risultato;
}
