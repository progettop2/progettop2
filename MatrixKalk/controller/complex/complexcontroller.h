#ifndef COMPLEXCONTROLLER_H
#define COMPLEXCONTROLLER_H
#include "../../model/complesso.h"
#include "../../model/eccezioni.h"
#include "../../view/complex/abstractcomplexgui.h"
#include <vector>
#include <QPushButton>
#include <QString>



class ComplexController {
private:
    Complesso *primo,*aux;
    abstractComplexGui* view;
    unsigned int operazioneRichiesta;
    bool appenaEseguito;
    ComplexController(const ComplexController&);
    ComplexController& operator =(const ComplexController&);

public:
    ComplexController(abstractComplexGui*);


    ~ComplexController();

    void inserisciNumero(QPushButton*);
    void del() const;
    void ac() const;
    QString root(int n);
    QString restituisciRisultato(QString R,QString I) const;
    void setOperazioneRichiesta(unsigned int);
    void esegui();
    void preparaOperazione(unsigned int);
    void updateComplesso(ComplexT,const QString&) const;
    int getOperazioneRichiesta() const;
    bool getAppenaEseguito() const;
    void setAppenaEseguito(bool);
    void argomento();
    void coniugato();
    void modulo();
};

#endif // COMPLEXCONTROLLER_H
