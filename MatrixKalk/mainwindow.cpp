#include "mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget* parent): QMainWindow(parent),mg(new MatrixGUI(this)),cg(new ComplexGUI(this)),wg(new QTabWidget(this)),
        layoutComplessi(new QHBoxLayout()),windowComplessi(new QWidget(this)),scroll(new QScrollArea(this)) {


    setFixedWidth(950);

    scroll->setBackgroundRole(QPalette::Light);
    scroll->setWidget(mg);
    scroll->setAlignment(Qt::AlignCenter);
    scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    wg->addTab(scroll,"Matrici");
    layoutComplessi->addWidget(cg,0,Qt::AlignCenter);

    windowComplessi->setLayout(layoutComplessi);
    wg->addTab(windowComplessi,"Complessi");
    setCentralWidget(wg);

}
