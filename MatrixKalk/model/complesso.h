#ifndef COMPLESSO_H
#define COMPLESSO_H
#include<iostream>
#include <cmath>
#include <vector>

/**
 * @brief Classe che modella i numeri complessi
 */
class Complesso {
public:
  //costruttore a 0,1,2 parametri di Complessi
  Complesso(double r = 0.0, double i = 0.0);

  //funzioni amiche
  friend Complesso operator+(const Complesso&, const Complesso&);
  friend Complesso operator-(const Complesso&, const Complesso&);
  friend Complesso operator*(const Complesso&, const Complesso&);
  friend Complesso operator/(const Complesso&, const Complesso&);
  friend std::ostream& operator<<(std::ostream&, const Complesso&);

  bool isNull() const;

  Complesso coniugato() const;
  double modulo() const;
  double argomento() const;
  Complesso pow(int) const;
  std::vector<Complesso> root(int) const;

  void setParteReale(double);
  void setParteImmaginaria(double);

  double getParteReale() const;
  double getParteImg()const;

private:
    double reale;
    double img;
};

#endif // COMPLESSO_H
