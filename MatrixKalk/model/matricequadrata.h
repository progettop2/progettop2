#ifndef MATRICEQUADRATA_H
#define MATRICEQUADRATA_H
#include "matrice.h"

class MatriceQuadrata: public Matrice {
public:
    MatriceQuadrata(std::vector< std::vector< double > >, int dimensione=1);

    MatriceQuadrata(int dimensione=1, double val=0.0);

    MatriceQuadrata matriceIdentita() const;

    double determinante()const;
    //calcola in O(1) il determinante
    double determinante2x2() const;

    bool checkInvertibile() const;

    MatriceQuadrata inversa() const;

    //ridefinizione di metodi ereditati
    MatriceQuadrata* operator-(const Matrice&) const;
    MatriceQuadrata* operator+(const Matrice&) const;
    MatriceQuadrata* operator *(const double) const;
    MatriceQuadrata* trasponi() const;
    MatriceQuadrata* eliminazioneGauss() const;

};

#endif // MATRICEQUADRATA_H
