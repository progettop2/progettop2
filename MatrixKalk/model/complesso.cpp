#include "complesso.h"
#include "eccezioni.h"

//Definizione del costruttore
Complesso::Complesso(double r, double i): reale(r), img(i) {}

//Somma
Complesso operator+(const Complesso& a, const Complesso& b) {
   return Complesso((a.reale+b.reale),(a.img+b.img));
}

//Sottrazione
Complesso operator-(const Complesso& a, const Complesso& b) {
    return Complesso((a.reale-b.reale),(a.img-b.img));
}

//Funzione che ritorna true se il numero complesso è nullo, altrimenti ritorna false
bool Complesso::isNull() const {
    if(reale==0.0 && img==0.0)
        return true;

return false;
}

/*Moltiplicazione
*dati due numeri complessi z1=a+ib, z2=c+id
*allora z1*z2=(ac-bd)+i(ad+bc)
* La moltiplicazione di due numeri complessi nulli genera un'eccezione
*/
Complesso operator*(const Complesso& z1, const Complesso& z2) {
    if(z1.isNull() && z2.isNull())
        throw complesso_error("Numero non Definito");

    double pReale = (z1.reale*z2.reale)-(z1.img*z2.img);
    double pImg = (z1.reale*z2.img)+(z1.img*z2.reale);
    return Complesso(pReale,pImg);
}

//Divisione
//dati due numeri complessi z1= a+ib, z2=c+id
//allora z1/z2=((ac+bd)+i(bc-ad))/(c^2+d^2)
Complesso operator/(const Complesso& z1, const Complesso& z2) {
    double pReale = (z1.reale*z2.reale)+(z1.img*z2.img);
    double pImg = (z1.img*z2.reale)-(z1.reale*z2.img);

    if(z2.isNull())
        throw controller_error("Divisione Per Zero");

    double denominatore = (z2.reale*z2.reale)+(z2.img*z2.img);
    return Complesso((pReale/denominatore),(pImg/denominatore));
}

//Fatta per provare degli output
std::ostream& operator<<(std::ostream& os, const Complesso& z1) {
    char a ='+';
    if(z1.img <= 0){
        a = ' ';
    }
    return os<<z1.reale<<" "<<a<<z1.img<<"i";
}


Complesso Complesso::coniugato() const {
    return Complesso(reale,(img*(-1.0)));
}

/*Dato il numero complesso z=a+ib, la funzione ritorna il modulo(la norma) cioè la misura del
*segmento OP con estremi l'origine
*del piano cartesiano e il punto z=(a,b)*/
double Complesso::modulo() const {
    //sqrt non puo' lanciare errori poichè per definizione si ha sotto radice un numero positivo
    return std::sqrt((reale*reale)+(img*img));
}

/*
 * Dato il numero complesso z=a+ib, la funzione ritorna l'angolo(argomento) tra il segmento OP
 * e l'asse dei reali.
 * Il calcolo è effettuato nel dominio [-PI_GRECO;PI_GRECO]
 * Se il numero complesso è uguale a 0 (parte reale ed immaginaria nulla)
 * allora la funzione lancia un'eccezione del tipo err_not_defined
 */
double Complesso::argomento() const {
    const double PI = std::atan(1.0)*4;
    if(!reale&&!img)
        throw complesso_error("Numero non Definito");
    if(!reale) {
        if(img<0)
            return -PI/2.0;
        return PI/2.0;
    }
    double arg = std::atan(img/reale);
    if(reale<0) {
        if(img<0)
            return arg-PI;
        return arg+PI;
    }
    return arg;
}

/*
 * Per l'elevamento a  potenza di un numero complesso si utilizza la forma esponenziale sulla quale
 * Calcolare la potenza e poi convertire il risultato in forma cartesiana
 */
Complesso Complesso::pow(int n) const {
    if(!n)
        throw complesso_error("Impossibile elevare a potenza nulla");

    double r = this->modulo();
    double theta = this->argomento();
    theta = theta*n;
    //bisogna gestire le eccezioni
    r = std::pow(r,n);
    double sin = std::sin(theta);
    double cos = std::cos(theta);

    return Complesso(r*cos,r*sin);
}

/**
 * @brief La funzione calcola le radici di un numero complesso.
 * @param int n
 * @return std::vector<Complesso>
 */
std::vector<Complesso> Complesso::root(int n) const {
    if(!n)
        throw complesso_error("Impossibile calcolare la radice 'nulla'");
    std::vector<Complesso> radici;
    double r = std::pow(this->modulo(),(1.0/n));
    double theta;
    const double PI = std::atan(1.0)*4;

    /*n potrebbe essere negativo per cui per poter calcolare le |n| radici bisogna prendere il valore assouluto di n per poter iterare.*/
    int k = std::abs(n);

    for(int i=0; i<k; ++i) {
        theta = (this->argomento()+(2*PI*i))/n;
        radici.push_back(Complesso(r*(std::cos(theta)),r*(std::sin(theta))));
    }

    return radici;
}

/**
 * @brief Complesso::getParteReale
 * @return restituisce la parte reale di un numero complesso
 */
double Complesso::getParteReale() const {
    return reale;
}

/**
 * @brief Complesso::getParteImg
 * @return restituisce la parte immaginaria di un numero complesso
 */
double Complesso::getParteImg() const {
    return img;
}

void Complesso::setParteReale(double r) {
    reale=r;
}

void Complesso::setParteImmaginaria(double i){
    img=i;
}
