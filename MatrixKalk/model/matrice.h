
#ifndef MATRICE_H
#define MATRICE_H
#include <iostream>
#include <vector>
#include <cmath>
class Matrice {
private:
    //std::vector bidimensionale per implementare la matrice.
    std::vector<std::vector<double> > elementi;
    //numero di righe e colonne della matrice.
    int nRighe;
    int nColonne;

    //metodo privato che serve a determinare il numero di elementi in un vector bidimensionale, viene usato nel costruttore di matrice
    int numeroElementi(std::vector< std::vector< double > >);
public:

    std::vector< double > getRiga(int) const;

    //costruttore di matrice, di default è una matrice 1x1.
    Matrice(std::vector< std::vector< double > >, int righe=1, int colonne=1);

    //costruttore a 0,1,2,3 argomenti -> costruisce una matrice con il valore specificato
    Matrice(int righe=1, int colonne=1, double val = 0.0);

    virtual Matrice* eliminazioneGauss() const;

    Matrice* concatena(const Matrice &) const;

    //moltiplicazione
    Matrice* operator*(const Matrice&) const;

    //prodotto per scalare
    virtual Matrice* operator*(const double) const;
    virtual Matrice* operator-(const Matrice&) const;
    virtual Matrice* operator+(const Matrice&) const;

    bool operator==(const Matrice&) const;

    bool operator!=(const Matrice&) const;

    //algoritmo di GaussJordan
    std::vector<Matrice*> eliminazioneGaussJordan() const;

    //rango
    int rango() const;

    //decomposizione PLU di una matricce
    std::vector<Matrice*> decomposizionePLU() const;


    //subscripting per oggetti costanti
    const std::vector< double >& operator[](const int) const;

    //oggetti non costanti
    std::vector< double >& operator[](const int);

    //distruttore virtuale
    virtual ~Matrice() {}

    //trasposizione di una matrice
    virtual Matrice* trasponi() const ;

    //metodi di utilità
    std::vector<std::vector<double> > getElementi() const;
    int getColonne() const;

    int getRighe() const;

    double getEL(int,int) const;

    //metodo stampa di debug
    void stampa() const;
    void setEl(double,int,int);
    void randomFill();
    void clear();
};

#endif // MATRICE_H
