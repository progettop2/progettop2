#ifndef ECCEZIONI_H
#define ECCEZIONI_H
#include <stdexcept>
#include <string>

using std::logic_error;
using std::runtime_error;

class matrice_error: public logic_error {
public:
  matrice_error(const std::string& errore): logic_error(errore) {}
};

class complesso_error: public logic_error {
public:
    complesso_error(const std::string& errore): logic_error(errore) {}

};

class controller_error: public runtime_error {
public:
    controller_error(const std::string& errore):runtime_error(errore) {}
};


#endif // ECCEZIONI_H
