#include "vettore.h"
#include "matrice.h"
#include "eccezioni.h"
#include<math.h>
#include <string>

Matrice Vettore::checkDimensioni(std::vector<std::vector<double> > ele, int dim, bool c) {
    //c==true => si vuole creare un vettore riga ma è stato inserito un vector colonna
    if(ele.size() > 1 && c==true)
        throw matrice_error("Errore nella creazione del vettore riga");

    if(ele.size() == 1 && c==false)
        throw matrice_error("Errore nella creazione del vettore colonna");
    if(c)
        return Matrice(ele,1,dim);
    else
        return Matrice(ele,dim,1);
}

Matrice Vettore::checkDimensioni(int dimensione, double val, bool r) {
    if(r)
        return Matrice(1,dimensione,val);
    else
        return Matrice(dimensione,1,val);


}

/**
 * @brief Vettore::Vettore costruisce un vettore colonna di default, altrimenti un vettore riga
 * @param ele rappresenta il primo parametro da passare al costruttore della classe base
 * @param dimensione rappresenta il secondo parametro da passare al costruttore della classe base
 * @param r determina se viene costruito un vettore riga o colonna;
 */
Vettore::Vettore(std::vector< std::vector< double > > ele, int dimensione, bool r):Matrice(Vettore::checkDimensioni(ele,dimensione,r)),riga(r) {}


Vettore::Vettore(int dimensione,double val,bool r): Matrice(Vettore::checkDimensioni(dimensione,val,r)),riga(r){}

bool Vettore::isColonna() const {
    return !(riga);
}
/**
 * @brief Vettore::vettoreColonna
 * @return true se il vettore è un vettore colonna
 */
bool Vettore::isRiga() const {
    return riga;
}

int Vettore::getDimensione()const {
    if(riga)
        return Matrice::getColonne();
    else
        return Matrice::getRighe();
}


Vettore* Vettore::trasponi() const {
    int col = Matrice::getColonne();
    int rig = Matrice::getRighe();
    std::vector< std::vector<double> > aux(col, std::vector<double>(rig));

    int ele = rig * col;

    for(int i = 0; i<rig; ++i)
        for(int j = 0; j < col; ++j)
            aux[j][i] = (*this)[i][j];

    return new Vettore(aux,ele,!(isRiga()));
}

double Vettore::norma() const {
    if(isRiga())
        throw matrice_error("La norma euclidea non è definita per vettori riga.");
    return sqrt(prodottoScalare(*this));
}

Vettore* Vettore::normalizzazione() const {
    if(isRiga())
        throw matrice_error("La normalizzazione non è definita per vettori riga.");
    double x = norma();
    if(x == 0){
        throw matrice_error("Impossibile normalizzare un vettore nullo.");
    }
    return (*this)*(1.0/x);
}



double Vettore::distanza(const Vettore& u) const {
    if((isColonna() != u.isColonna())|| (isRiga() != u.isRiga()) || (getDimensione()!=u.getDimensione()))
        throw matrice_error("Le dimensioni dei vettori non sono identiche.");
    Vettore* aux = (*this) - u;
    return aux->norma();
}

Vettore* Vettore::operator-(const Matrice& m) const {
    const Vettore* u = dynamic_cast<const Vettore*>(&m);
    if(u) {

        bool isC = isColonna();
        int n = getDimensione();

        if((n != u->getDimensione()) || (isC != u->isColonna()))
            throw matrice_error("Dimensioni dei due vettori non compatibili.");

        int col = Matrice::getColonne();
        int rig = Matrice::getRighe();
        std::vector< std::vector<double> > aux(rig, std::vector<double>(col));

        if(isRiga()) {
            for(int i = 0; i < n; ++i) {
                   aux[0][i] = (*this)[0][i] - (*u)[0][i];
            }
        }
        else {
            for(int i = 0; i < n; ++i) {
                   aux[i][0] = (*this)[i][0] - (*u)[i][0];
            }
        }

        return new Vettore(aux,n,!(isC));
    }
    else
        throw matrice_error("B non è un vettore.");
}

Vettore* Vettore::operator+(const Matrice& m) const {
    const Vettore* u = static_cast<const Vettore*>(&m);
    if(u) {
        bool isC = isColonna();
        int n = getDimensione();
        if((n != u->getDimensione()) || (isC != u->isColonna()))
            throw matrice_error("Dimensioni dei due vettori non compatibili.");

        std::vector< std::vector<double> > aux(getRighe(), std::vector<double>(getColonne()));
        if(isRiga()) {
            for(int i = 0; i < n; ++i) {
                   aux[0][i] = (*this)[0][i] + (*u)[0][i];
            }
        }
        else {
            for(int i = 0; i < n; ++i) {
                   aux[i][0] = (*this)[i][0] + (*u)[i][0];
            }
        }

        return new Vettore(aux,n,!(isC));
    }
    else
        throw matrice_error("B non è un vettore.");
}

Vettore* Vettore::operator *(const double scalare) const {
    Vettore* risultato = new Vettore(*this);
    if(isColonna()) {
        for(int i = 0; i < getRighe(); ++i)
               (*risultato)[i][0] *= scalare;
    }
    
    if(isRiga()) {
        for(int i = 0; i < getColonne(); ++i)
            (*risultato)[0][i] *= scalare;
    }

    return risultato;
}    



Vettore* Vettore::eliminazioneGauss() const {
    return static_cast<Vettore*>(Matrice::eliminazioneGauss());
}


double Vettore::prodottoScalare(const Vettore &y) const {
    Matrice* ts=this->trasponi();
    Matrice* ris =(*ts)*y;


    double prodotto=ris->getEL(0,0);

    delete ris;
    return prodotto;
}



double Vettore::angolo(const Vettore &v, bool dec) const {
    if(isRiga())
        throw matrice_error("Non è possibile calcolare l'angolo, A non è un vettore colonna.");
    if(v.isRiga())
        throw matrice_error("Non è possibile calcolare l'angolo, B non è un vettore colonna.");
    if(getDimensione()!=v.getDimensione())
        throw matrice_error("Non è possibile calcolare l'angolo, le dimensioni dei due vettori non sono compatibili.");
    double x = norma();
    double y = v.norma();
    if(x == 0)
        throw matrice_error("Non è possibile calcolare l'angolo, A è un vettore nullo");
    if(y == 0)
        throw matrice_error("Non è possibile calcolare l'angolo, B è un vettore nullo");

    if(dec)
        return ((std::acos((prodottoScalare(v))/(norma()*v.norma())))*180)/4;

    return std::acos((prodottoScalare(v))/(norma()*v.norma()));
}
