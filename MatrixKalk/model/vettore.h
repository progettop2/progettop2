#ifndef VETTORE_H
#define VETTORE_H
#include <list>
#include "matrice.h"

class Vettore: public Matrice {
private:
    bool riga;
    static Matrice checkDimensioni(std::vector< std::vector < double > >, int, bool);
    static Matrice checkDimensioni(int, double, bool);

public:
    //Costruttori
    Vettore(std::vector< std::vector< double > >, int = 1, bool = true);
    Vettore(int = 1,double = 0.0,bool = true);

    double norma() const;
    double prodottoScalare(const Vettore &) const;
    int getDimensione() const;

    bool isColonna() const;
    bool isRiga() const;
    Vettore* trasponi() const;
    double distanza(const Vettore&) const;
    Vettore* operator-(const Matrice&) const;
    Vettore* operator+(const Matrice&) const;
    Vettore* operator*(const double) const;
    Vettore* eliminazioneGauss() const;
    Vettore* normalizzazione() const;

    double angolo(const Vettore&, bool dec) const;

};

#endif // VETTORE_H
