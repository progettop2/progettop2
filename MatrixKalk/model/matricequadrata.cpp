#include "matricequadrata.h"
#include "eccezioni.h"
#include <iostream>
#include <vector>

//Costruttore di matrice quadrata
MatriceQuadrata::MatriceQuadrata(std::vector< std::vector< double > > ele, int dimensioni):Matrice(ele,dimensioni,dimensioni) {}

MatriceQuadrata::MatriceQuadrata(int dimensioni, double val):Matrice(dimensioni,dimensioni,val) {}

double MatriceQuadrata::determinante() const {
    if(getColonne() == 2)
        return determinante2x2();
    else {
        int permutazioni = 0;
        int moltiplicatore = 1.0;
        std::vector<Matrice*> PLU = decomposizionePLU();
        int dimMperm = (PLU[0])->getRighe();
        for(int i = 0; i < dimMperm; ++i) {
            if(((*(PLU[0]))[i][i]) == 0 )
                permutazioni++;
        }

        Matrice* U = PLU[2];
        int n = getColonne();
        double det = 1.0;
        for(int i = 0; i < n; ++i) {
           det = det*(*U)[i][i];
        }
        if(permutazioni%2 != 0){
            moltiplicatore = -1.0;
        }

        //Pulisco il garbage
        for (std::vector< Matrice* >::iterator it = PLU.begin() ; it != PLU.end(); ++it) {
            delete (*it);
        }

        return moltiplicatore * det;
    }
}

/**
 * @brief MatriceQuadrata::matriceIdentità
 * @param i numero delle righe/colonne
 * @return Matrice quadrata avente le dimensioni della matrice d'invocazione e i membri sulla diagonale=1 e tutti gli altri uguale a 0
 */
MatriceQuadrata MatriceQuadrata::matriceIdentita() const {

    MatriceQuadrata mc(getColonne(),0.0);

    for(int j=0; j < mc.getColonne(); ++j){
        mc.setEl(1,j,j);
    }
    return mc;
}


double MatriceQuadrata::determinante2x2() const {
    return (getEL(0,0)*getEL(1,1)) - (getEL(0,1)*getEL(1,0));
}


bool MatriceQuadrata::checkInvertibile() const {
    return rango() == getColonne();
}

/**
 * @brief MatriceQuadrata::inversa
 * @return se la matrice d'invocazione è invertibile, ritorna la matrice inversa della matrice d'invocazione
 */
MatriceQuadrata MatriceQuadrata::inversa() const{
    if(!checkInvertibile()) {
        throw matrice_error("La matrice non è invertibile.");
    }
    MatriceQuadrata i = matriceIdentita();
    Matrice* m = concatena(i);

    std::vector<Matrice*> aux = m->eliminazioneGaussJordan();
    delete m;
    m = aux[1];

    MatriceQuadrata R(getRighe());

    //copio i risultati in R
    int colonne = getColonne();
    for(int i=0; i < getRighe(); ++i) {
        for(int j=0; j < getRighe(); ++j) {
            R[i][j]=(*m)[i][j+colonne];
        }
    }
    delete aux[0];
    delete aux[1];
    return R;
}



MatriceQuadrata* MatriceQuadrata::operator *(const double scalare) const {
    MatriceQuadrata* risultato = new MatriceQuadrata(*this);
    int n = getRighe();
    for(int i = 0; i < n; ++i)
       for(int j = 0; j < n; ++j)
           (*risultato)[i][j] = (*risultato)[i][j] * scalare;
    return risultato;
}

MatriceQuadrata* MatriceQuadrata::operator+(const Matrice& m) const {
    if(dynamic_cast<const MatriceQuadrata*>(&m)) {
        if((getRighe() != m.getRighe()) || (getColonne() != m.getColonne())) {
            throw matrice_error("Le dimensioni delle due matrici non sono identiche.");
        }
        int n = getRighe();
        std::vector< std::vector<double> > aux(n, std::vector<double>(getColonne()));

        for(int i=0; i<n; ++i) {
            for(int j=0; j<n; ++j) {
                 aux[i][j] = (*this)[i][j] + m[i][j];
            }
        }
        return new MatriceQuadrata(aux,n);
    }
    else
        throw matrice_error("B non è una matrice quadrata.");
}


MatriceQuadrata* MatriceQuadrata::operator-(const Matrice& m) const {
    if(dynamic_cast<const MatriceQuadrata*>(&m)) {
        if((getRighe() != m.getRighe()) || (getColonne() != m.getColonne())) {
            throw matrice_error("Le dimensioni delle due matrici non sono identiche.");
        }

        int n = getRighe();
        std::vector< std::vector<double> > aux(n, std::vector<double>(n));

        for(int i=0; i<n; ++i) {
            for(int j=0; j<n; ++j) {
                 aux[i][j] = (*this)[i][j] - m[i][j];
            }
        }
        return new MatriceQuadrata(aux,n);
    }
    throw matrice_error("B non è una matrice quadrata.");
}


MatriceQuadrata* MatriceQuadrata::trasponi() const {
    int n = getRighe();
    std::vector< std::vector<double> > aux(n, std::vector<double>(n));

        for(int i = 0; i<n; ++i)
            for(int j = 0; j < n; ++j)
                aux[j][i] = (*this)[i][j];

        return new MatriceQuadrata(aux,n);
}

MatriceQuadrata* MatriceQuadrata::eliminazioneGauss() const {
    return static_cast<MatriceQuadrata*>(Matrice::eliminazioneGauss());
}


