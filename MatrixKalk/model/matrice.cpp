#include "matrice.h"
#include "eccezioni.h"
#include<vector>
#include<iostream>
#include <cmath>
#include <cstdlib>


//MATRICE.CPP
Matrice::Matrice(std::vector< std::vector< double > > coeff, int righe, int colonne): elementi(coeff), nRighe(righe), nColonne(colonne) {
    if(righe <= 0 || colonne <= 0) {
        //eccezione
        throw matrice_error("Dimensioni non valide");
    }
    /*Controllo che il vettore di vettori coeff abbia più (o meno) elementi di righe*colonne, in caso affermativo
    la matrice non è ben formata per cui lancio un'eccezione*/
    if(numeroElementi(coeff) != (righe*colonne)) {
        throw matrice_error("Il vettore ha più o meno elementi di quanti dovrebbero essere nella matrice");
    }
    else{
        elementi.resize(righe);
        for (int i = 0; i < righe; i++) {
            elementi[i].resize(colonne);
        }
    }
}

Matrice::Matrice(int righe, int colonne, double val):nRighe(righe), nColonne(colonne) {
    if(righe <= 0 || colonne <= 0) {
        //eccezione
        throw matrice_error("Dimensioni non valide");
    }
    else{
        elementi.resize(righe);
        for (int i = 0; i < righe; i++) {
            elementi[i].resize(colonne);
            std::fill(elementi[i].begin(), elementi[i].end(), val);
        }
    }
}

/**
 * @brief La funzione ritorna il numero di elemente di un array a due dimensioni
 * @param std::vector<std::vector<double> > elements, array a due dimensioni
 * @return int
 */
int Matrice::numeroElementi(std::vector< std::vector<double> > elements) {
    int sommaVector2D = 0;
    for(unsigned int i = 0; i < elements.size(); ++i)
        sommaVector2D += elements[i].size();

    return sommaVector2D;
}

std::vector< double > Matrice::getRiga(int i) const {
    if(i >= nRighe)
        throw matrice_error("Out of bounds");

    return elementi[i];
}

const std::vector< double >& Matrice::operator[](const int i) const {
    return elementi[i];
}


std::vector< double >& Matrice::operator[](const int i) {
    return elementi[i];
}


/**
  @brief Matrice::operator + (somma tra 2 matrici)
  @param m di tipo Matrice
  @return Matrice, ritorna per valore il risultato della somma tra 2 matrici
 */
Matrice* Matrice::operator+(const Matrice& m) const {
    if((nRighe != m.nRighe) || (nColonne != m.nColonne)) {
        throw matrice_error("Le dimensioni delle due matrici non sono identiche");
    }

    std::vector< std::vector<double> > aux(nRighe, std::vector<double>(nColonne));
    for(int i=0; i<nRighe; ++i) {
        for(int j=0; j<nColonne; ++j) {
             aux[i][j] = (*this)[i][j] + m[i][j];
        }
    }
    return new Matrice(aux,nRighe,nColonne);
}

/**
 * @brief Matrice::operator - (differenza tra 2 matrici)
 * @param Matrice m
 * @return Matrice
 */
Matrice* Matrice::operator-(const Matrice& m) const {
    if((nRighe != m.nRighe) || (nColonne != m.nColonne)) {
        throw matrice_error("Le dimensioni delle due matrici non sono identiche");
    }        
    std::vector< std::vector<double> > aux(nRighe, std::vector<double>(nColonne));

    for(int i=0; i<nRighe; ++i) {
        for(int j=0; j<nColonne; ++j) {
            aux[i][j] = (*this)[i][j] - m[i][j];
        }
    }
    return new Matrice(aux,nRighe,nColonne);
}

//moltiplicazione tra 2 matrici A(n*m) e B(k*p) se m != k allora non è possibile eseguire la moltiplicazione
Matrice* Matrice::operator*(const Matrice& m) const {
    if(nColonne != m.nRighe) {
        throw matrice_error("Le colonne della prima matrice non sono uguali alle righe della seconda matrice");
    }
    //creo una matrice sullo heap nulla di dimensioni n*p
    std::vector< std::vector<double> > risultato(nRighe, std::vector<double>(m.nColonne));

    for(int i = 0; i < nRighe; ++i) {
        for(int j = 0; j < m.nColonne; ++j) {
            double aux = 0;
            for(int k = 0; k < m.nRighe; ++k) {
                aux = aux + (*this)[i][k] * m[k][j];
            }
            risultato[i][j] = aux;
        }
    }
    return  new Matrice(risultato,nRighe,m.nColonne);
}


//prodotto per scalare
Matrice* Matrice::operator*(const double scalare) const {
   Matrice* risultato =  new Matrice(*this);
    for(int i = 0; i < nRighe; ++i)
       for(int j = 0; j < nColonne; ++j)
           (*risultato)[i][j] = (*risultato)[i][j] * scalare;
    return risultato;
}


/**
 * @brief Matrice::concatena
 * @return Matrice risultato della concatenazione (a|b)
 */
Matrice* Matrice::concatena(const Matrice& b) const {
    if(nRighe != b.nRighe) {
        throw matrice_error("Le matrici non sono concatenabili.");
    }

    Matrice* ris = new Matrice(nRighe,nColonne+b.nColonne);
    //riempio la prima metà della matrice
    for(int i=0; i < nRighe; ++i) {
        for(int j=0; j < nColonne; ++j) {

            (*ris).elementi[i][j]=elementi[i][j];
        }
    }
    //riempio la seconda metà della matrice
    for(int i=0; i < b.nRighe; ++i) {
        for(int j=0; j < b.nColonne; ++j) {
            (*ris).elementi[i][nColonne+j]=b.elementi[i][j];
        }
    }
    return ris;
}


Matrice* Matrice::trasponi() const {
    std::vector< std::vector<double> > aux(nColonne, std::vector<double>(nRighe));

    for(int i = 0; i<nRighe; ++i)
        for(int j = 0; j < nColonne; ++j)
            aux[j][i] = (*this)[i][j];

    return new Matrice(aux,nColonne,nRighe);
}


int Matrice::getColonne() const {
    return nColonne;
}
int Matrice::getRighe() const{
    return nRighe;
}

bool Matrice::operator !=(const Matrice & m) const{
    if(nRighe!=m.nRighe||nColonne!=m.nColonne) return true;
    for(int i = 0; i<nRighe; ++i) {
        for(int j = 0; j < nColonne; ++j) {
            if(elementi[i][j]!=m.elementi[i][j])
            return true;
        }
    }
return false;

}
bool Matrice::operator ==(const Matrice& m) const {
if(nRighe!=m.nRighe||nColonne!=m.nColonne) return false;
    for(int i = 0; i<nRighe; ++i) {
        for(int j = 0; j < nColonne; ++j) {
            if(elementi[i][j]!=m.elementi[i][j])
            return false;
        }
    }
return true;
}

/**
 * @brief La funzione calcola il rango di una matrice applicando il metodo
 *  di eliminazione di Gauss con pivoting parziale, essenziale per la stabilità
 *  numerica dell'eliminazione di Gauss.
 * @return ritorna il rango della matrice
 */
int Matrice::rango() const {
      std::vector<Matrice*> aux = decomposizionePLU();
      Matrice* gauss = aux[2];
      int ris = 0;
      bool pivotTrovato = false;
      for(int i = 0; i < nRighe; ++i) {
          pivotTrovato = false;
          for(int j = 0; j < nColonne && !pivotTrovato; ++j) {
              if((*gauss)[i][j] != 0) {
                  ris++;
                  pivotTrovato = true;
              }
          }
      }

      //pulisco garbage
      for (std::vector< Matrice* >::iterator it = aux.begin() ; it != aux.end(); ++it) {
          delete (*it);
      }
      return ris;
}


void Matrice::stampa() const {
    std::cout<<"-----------"<<std::endl;
    for(int i = 0; i<nRighe; ++i) {
        for(int j = 0; j < nColonne; ++j) {
          if(elementi[i][j] >= 10 || elementi[i][j]<0)
            std::cout<<elementi[i][j]<<"|";
          else
             std::cout<<elementi[i][j]<<" |";
        }
       std::cout<<std::endl;
    }
    std::cout<<"-----------"<<std::endl;
}

void Matrice::setEl(double val, int i, int j) {
    elementi[i][j] = val;
}

double Matrice::getEL(int i, int j) const{
    return elementi[i][j];
}

//metodo di utility: riempie una matrice di numeri random da 0 a 20
void Matrice::randomFill() {
    for(int i = 0; i<nRighe; ++i) {
        for(int j = 0; j < nColonne; ++j) {
          double k= rand() % 20;
          elementi[i][j]=k;
        }
    }
}

//utility: setta tutto a 0
void Matrice::clear(){
    for(int i = 0; i<nRighe; ++i) {
        for(int j = 0; j < nColonne; ++j) {
          elementi[i][j]=0;
        }
    }
}


std::vector<Matrice*> Matrice::decomposizionePLU() const {
    /* Vector aux rappresenta una riga della matrice, è ausialiario
     * poichè permette di fare gli scambi di riga
     */

    int dimL  = nRighe;

    //Non possono lanciare eccezioni: dimL > 0
    Matrice* P = new Matrice(dimL, dimL,0);
    Matrice* L = new Matrice(dimL, dimL,0);
    Matrice* U = new Matrice(*this);

    std::vector<double> aux(nColonne);

    //k serve nel caso in cui il pivot sia uguale a zero, in questo caso  non si aumenta k in modo tale da
    //prende come pivot l'elemento successivo all'elemento nullo
    int k = 0;

    for(int i = 0; i < dimL; ++i) {
        (*P)[i][i] = 1;
        (*L)[i][i] = 1;
    }
    //Matrice ausiliaria che permette di salvare i moltiplicatori trovati durante l'algoritmo di eliminazione di Gauss



    //scorro le righe della matrice
    for(int i = 0; i < nRighe && k < nColonne; ++i) {
        // maxRiga indica la riga dove è stato trovato il massimo elemento (maxEl)
        int maxRiga = k;
        double maxEl = std::abs((*U)[k][i]);

        //confronto gli elementi successivi in colonna con il massimo trovato finora
        for(int j = i+1; j < nRighe; ++j) {
            if(std::abs((*U)[j][i]) > maxEl) {
                maxEl = std::abs((*U)[j][i]);
                maxRiga = j;
            }
        }
        if(maxEl) {

            /* a questo punto ho trovato l'elemento massimo e la sua riga
             * ora bisogna scambiare la riga "massima" con la riga corrente
             * Pivoting parziale
             */
            if(maxRiga != i) {
                aux = (*U)[i];
                (*U)[i] = (*U)[maxRiga];
                (*U)[maxRiga] = aux;

                aux = (*P)[i];
                (*P)[i] = (*P)[maxRiga];
                (*P)[maxRiga] = aux;
            }
            //Ho scambiato le righe ora in i si trova la riga massima

            /* Adesso bisogna eseguire operazioni di pivot sull'elemento
             * Cioè porre a zero tutti gli elementi sotto al pivot della stessa colonna
             */
            for(int j = k+1; j < nRighe; ++j) {
                 double moltiplicatore = -((*U)[j][i]/maxEl);

                 for(int s = 0; s < nColonne; ++s) {
                     if(s == i) {
                         (*U)[j][s] = 0;
                         (*L)[j][s] = moltiplicatore * (-1.0);
                     }
                     else {
                         (*U)[j][s] += moltiplicatore*((*U)[k][s]);
                     }
                 }
            }
            k++;
        }
    }

    std::vector<Matrice*> PLU;
    PLU.push_back(P);
    PLU.push_back(L);
    PLU.push_back(U);
    return PLU;
}


/**
 * @brief Matrice::eliminazioneGaussJordan La funzione applica l'algoritmo di Gauss-Jordan ad una matrice
 * @return Matrice ritorna per valore una matrice ridotta alla forma di Gauss-Jordan
 */
std::vector<Matrice*> Matrice::eliminazioneGaussJordan() const {
    std::vector<Matrice*> aux = this->decomposizionePLU();
    delete aux[1];
    std::vector<Matrice*> GJ = {aux[0],aux[2]};

    //Percorro dall'ultima riga la matrice e cerco il pivot
    //quando trovo il pivot in posizione [i][j] (se c'è) allora procedo ad porre a 1 il pivot e annullare tutti gli elementi della
    //colonna j sopra il pivot

    for(int i=((GJ[1])->getRighe()-1); i>=0; --i) {
        bool pivotTrovato = false;
        double divisore = 1.0;
        for(int j=0; j < (GJ[1])->getColonne(); ++j) {
            // ho trovato il pivot
            if((*(GJ[1]))[i][j] != 0 && !pivotTrovato) {
                divisore = (*(GJ[1]))[i][j];
                pivotTrovato = true;

                for(int z=j; z < (GJ[1])->getColonne(); ++z) {
                    (*(GJ[1]))[i][z] /= divisore;
                }
                for(int k=i-1; k>=0; --k) {
                    double sott = (*(GJ[1]))[k][j];
                    for(int s = j; s < (GJ[1])->getColonne(); ++s) {
                            (*(GJ[1]))[k][s] -= sott*((*(GJ[1]))[i][s]);
                    }
                }
            } 
        }
    }

    return GJ;
}

Matrice* Matrice::eliminazioneGauss() const {
   std::vector< Matrice* >aux = decomposizionePLU();
   delete aux[0];
   delete aux[1];
   return aux[2];
}

std::vector< std::vector<double> > Matrice::getElementi() const {
    return elementi;
}

