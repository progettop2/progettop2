#include "mainwindow.h"
#include <QApplication>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <vector>
#include "model/eccezioni.h"
#include "model/matrice.h"
#include "model/matricequadrata.h"
#include "controller/matrix/matrixcontroller.h"
#include "model/vettore.h"

using std::cout;
using std::vector;
using std::endl;

int main(int argc, char * array []) {

    QApplication a(argc, array);
    MainWindow w;
    w.show();
    return a.exec();
}
