#ifndef MATRIXVIEW_H
#define MATRIXVIEW_H

#include <QWidget>
#include <QGridLayout>
#include <QLineEdit>
#include <vector>
#include <QDebug>
#include <QDoubleValidator>
#include <QShortcut>
using std::vector;

class MatrixView : public QWidget {
    Q_OBJECT
private:
    QGridLayout *m;
    vector< vector< QLineEdit* > > celle;
    QDoubleValidator* validator;
    bool modificato;
    bool checkFocus()const;
    QShortcut *up;
    QShortcut *down;
    QShortcut *left;
    QShortcut *right;

public:
    explicit MatrixView(QWidget *parent = 0, int =1, int=1);
    QLineEdit*& getLineEdit(int,int);
    void setReadOnly(bool);
    void setModifica(bool);
    bool getModifica()const;
    vector< vector <QLineEdit*> > getCelle() const;
    int getRighe() const;
    int getColonne() const;
    void trasferisci(vector< vector<QLineEdit*> >);
    vector< QLineEdit* >& operator[](const int);

signals:

public slots:
    void celleIsModified();
    void move();
};

#endif // MATRIXVIEW_H
