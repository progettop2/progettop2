#include "matrixdim.h"

MatrixDim::MatrixDim(QWidget *parent): QWidget(parent),righe(new QSpinBox),colonne(new QSpinBox),
    ok(new QPushButton("Ok")),layout(new QGridLayout),x(new QLabel("X")) {

    setFixedWidth(130);
    righe->setFixedWidth(40);
    colonne->setFixedWidth(40);
    righe->setValue(5);
    colonne->setValue(5);

    righe->setMinimum(1);
    righe->setMaximum(5);
    righe->setWrapping(true);

    colonne->setMinimum(1);
    colonne->setMaximum(5);
    colonne->setWrapping(true);

    //x->setFixedSize(40,20);
    x->setAlignment(Qt::AlignCenter);

    layout->addWidget(righe,0,0);
    layout->addWidget(x,0,1);
    layout->addWidget(colonne,0,2);
    layout->addWidget(ok,1,1);

    setLayout(layout);

}

QPushButton*& MatrixDim::getOkButton() {
    return ok;
}

int MatrixDim::getRigheValue()const {
    return righe->value();
}


int MatrixDim::getColonneValue()const {
   return colonne->value();
}
