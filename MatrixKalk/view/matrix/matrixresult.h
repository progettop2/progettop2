#ifndef MATRIXRESULT_H
#define MATRIXRESULT_H

#include <QWidget>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QString>
#include <QLabel>
#include <QVBoxLayout>
#include "matrixview.h"

using std::vector;

class MatrixResult : public QWidget {
    Q_OBJECT
private:
    QPushButton *trasferisci;
    QComboBox *selezione;
    QVBoxLayout* layout;
    QLabel* title;
    unsigned int righe, colonne;
    MatrixView *result;
public:
    explicit MatrixResult(QWidget *parent = 0, unsigned int r = 1,unsigned int = 1 );
    QPushButton*& getTrasferisci();
    void disableOk(bool);
    int getIndexSelected() const;
    vector<QLineEdit*>& operator[](const int i);
    vector< vector<QLineEdit*> > getElementi() const;
    int getRighe() const;
    int getColonne() const;
    QString getSelezione() const;
    void setTitle(QString);


signals:

public slots:
};

#endif // MATRIXRESULT_H
