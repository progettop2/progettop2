#ifndef MATRIXDIM_H
#define MATRIXDIM_H

#include <QFrame>
#include <QDialogButtonBox>
#include <QSpinBox>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>


class MatrixDim: public QWidget {
public:
    explicit MatrixDim(QWidget* =0);
    QPushButton*& getOkButton();
    int getRigheValue() const;
    int getColonneValue() const;

private:
    QSpinBox *righe;
    QSpinBox *colonne;
    QPushButton *ok;
    QGridLayout *layout;
    QLabel *x;

};

#endif // MATRIXDIM_H
