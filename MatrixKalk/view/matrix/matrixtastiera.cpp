#include "matrixtastiera.h"

MatrixTastiera::MatrixTastiera(QWidget *parent) : QWidget(parent),layout(new QGridLayout),validatore(new QDoubleValidator(this)),prodotto(new QLineEdit) {

    setFixedHeight(170);
    for(int i=0; i <= 9; i++) {
        QPushButton *b = new QPushButton;
        buttons.push_back(b);
    }
    //prodotto->setFixedWidth(80);

    buttons[0]->setText("El. Gauss-Jordan");
    buttons[1]->setText("Dec. PLU");
    buttons[2]->setText("Trasposta");

    //NECESSARIA MATRICE QUADRATA
    buttons[3]->setText("Determinante");
    buttons[4]->setText("Inversa");
    buttons[5]->setText("Rango");

    //NECESSARIO VETTORE
    buttons[6]->setText("Norma Euclidea");

    buttons[7]->setText("Normalizzazione");
    buttons[9]->setText("Eliminazione di Gauss");
    buttons[8]->setText("Prodotto per scalare");

    for(unsigned int i=0; i < buttons.size()-1; i++) {
        layout->addWidget(buttons[i],i/2,i%2);
    }
    layout->addWidget(prodotto,4,1,1,1,Qt::AlignLeft);
    layout->addWidget(buttons[9],5,0);
    prodotto->setStyleSheet("padding-right:10 px;");
    setLayout(layout);
    this->adjustSize();

}

QPushButton*& MatrixTastiera::getButton(int i) {
    return buttons[i];
}

void MatrixTastiera::enableButtons(int righe, int colonne){

    setEnabled(true);

    if(righe==colonne) {
        buttons[6]->setEnabled(false);
        buttons[7]->setEnabled(false);
    }

    if(righe==1) {
        buttons[3]->setEnabled(false);
        buttons[4]->setEnabled(false);
        buttons[5]->setEnabled(false);
        buttons[6]->setEnabled(false);
        buttons[7]->setEnabled(false);
    }
    if(colonne == 1) {
        buttons[3]->setEnabled(false);
        buttons[4]->setEnabled(false);
        buttons[5]->setEnabled(false);
    }


   if(righe!=1 && colonne!=1 && righe!=colonne) {
        buttons[3]->setEnabled(false);
        buttons[4]->setEnabled(false);
        buttons[5]->setEnabled(false);
        buttons[6]->setEnabled(false);
        buttons[7]->setEnabled(false);
    }
}


void MatrixTastiera::setEnabled(bool a) {

    for(unsigned int i=0; i < buttons.size(); i++) {
        buttons[i]->setEnabled(a);
    }

}

double MatrixTastiera::getScalare() const {
    return prodotto->text().toDouble();
}
