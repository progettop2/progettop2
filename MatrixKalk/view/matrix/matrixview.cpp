#include "matrixview.h"

MatrixView::MatrixView(QWidget *parent, int righe, int colonne) : QWidget(parent),m(new QGridLayout),celle(righe*colonne),
    validator(new QDoubleValidator(this)), modificato(false),up(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_8), this)),
    down(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_2), this)),left(new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_4), this)),
    right( new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_6), this)) {

    validator->setDecimals(2);


    celle.resize(righe);
    for(int i = 0; i < righe; i++) {
        celle[i].resize(colonne);
    }

    for(int i=0; i < righe; i++)
    {
        for (int f=0; f < colonne; f++)
        {
            celle[i][f] = new QLineEdit(this);
            celle[i][f]->setFixedSize(50,30);
            m->addWidget(celle[i][f], i, f);
            celle[i][f]->setStyleSheet("padding: 3px;");
            celle[i][f]->setPlaceholderText(QString::number(0.0));
            celle[i][f]->setValidator(validator);
            connect(celle[i][f],&QLineEdit::textChanged,this,&MatrixView::celleIsModified);
        }
    }
    m->setSpacing(1.2);

    setLayout(m);

    connect(up,&QShortcut::activatedAmbiguously,this,&MatrixView::move);
    connect(down,&QShortcut::activatedAmbiguously,this,&MatrixView::move);
    connect(right,&QShortcut::activatedAmbiguously,this,&MatrixView::move);
    connect(left,&QShortcut::activatedAmbiguously,this,&MatrixView::move);



}

QLineEdit*& MatrixView::getLineEdit(int i,int j) {
    return celle[i][j];
}

void MatrixView::setReadOnly(bool a=true) {

    for(unsigned int i=0; i < celle.size(); i++ )
        for(unsigned int j=0; j < celle[0].size(); j++)
            celle[i][j]->setReadOnly(a);

}


void MatrixView::celleIsModified(){
    modificato=true;
}


void MatrixView::setModifica(bool a){
    modificato=a;
}


bool MatrixView::getModifica() const{
    return modificato;
}


std::vector< std::vector <QLineEdit*> > MatrixView::getCelle() const{
    return celle;

}

int MatrixView::getRighe() const{

    return celle.size();
}


int MatrixView::getColonne() const{

    return celle[0].size();
}


void MatrixView::move() {
    QObject *sender=QObject::sender();
    if(checkFocus()) {
    //cerco l'oggetto che ha il focus
        int riga,colonna;

        for(int i=0; i < getRighe(); i++) {
                for(int j=0; j< getColonne(); j++) {
                    if(celle[i][j]->hasFocus()) {
                        riga=i;
                        colonna=j;
                    }
            }
        }

        if(sender==up){
            if(riga !=0)
                celle[riga-1][colonna]->setFocus();
            return;
        }

        if(sender==left){
            if(colonna !=0)
                celle[riga][colonna-1]->setFocus();
            return;
        }

        if(sender==down){
            if(riga !=getRighe()-1)
                celle[riga+1][colonna]->setFocus();
            return;
        }

        if(sender==right){
            if(colonna != getColonne()-1)
                celle[riga][colonna+1]->setFocus();
            return;
        }
    }
}


bool MatrixView::checkFocus() const {
    for(int i=0; i < getRighe(); i++)
        for(int j=0; j< getColonne(); j++)
             if(celle[i][j]->hasFocus())
                    return true;
    return false;
}

void MatrixView::trasferisci(vector< vector<QLineEdit*> > elementi) {
    int righe = elementi.size();
    int colonne = elementi[0].size();
    celle.clear();
    celle.resize(righe);

    for(int i = 0; i < righe; ++i ) {
        for(int j = 0; j < colonne; ++j) {
            celle[i].resize(colonne);
            celle[i][j] = elementi[i][j];
        }
    }
    return;
}

vector< QLineEdit* >& MatrixView::operator[](const int i) {
    return celle[i];
}
