#include "matrixgui.h"
#include "../../controller/matrix/matrixcontroller.h"
#include <QDebug>
#include <QChar>
#include <typeinfo>

MatrixGUI::MatrixGUI(QWidget* parent):abstractMatrixGUI(parent), headerFirstMatrix(new MatrixDim), headerSecondMatrix(new MatrixDim),
    firstMatrix(new MatrixView(this,5,5)), secondMatrix(new MatrixView(this,5,5)) ,
    tastieraFirstArg(new MatrixTastiera(this)),tastieraSecondArg(new MatrixTastiera(this)),l(new QVBoxLayout),
    lFirst(new QLabel("A")),lSecond(new QLabel("B")),resultL(new MatrixResult(this,5,5)),resultC(new MatrixResult(this,5,5)),resultR(new MatrixResult(this,5,5)),layoutMatrixContainer(new QHBoxLayout),
    layoutButtonsContainer(new QVBoxLayout()),layoutHeaderContainer(new QGridLayout),
    layoutLabelContainer(new QHBoxLayout),
    layoutKeyboardContainer(new QHBoxLayout),
    layoutResultContainer(new QHBoxLayout),
    titolo(new QLabel(" ")) ,
    controller(this) {



    lFirst->setAlignment(Qt::AlignCenter);
    lSecond->setAlignment(Qt::AlignCenter);

    lFirst->setStyleSheet("font-size: 15pt;");
    lSecond->setStyleSheet("font-size: 15pt;");
    QString theta =  QString::fromWCharArray(L"\u03D1");

    buttonsTwoArgument[0] = new QPushButton("A + B");
    buttonsTwoArgument[1] = new QPushButton("A - B");
    buttonsTwoArgument[2] = new QPushButton("A * B");
    buttonsTwoArgument[3] = new QPushButton("d(A,B)");
    buttonsTwoArgument[4] = new QPushButton(theta+"(A,B)",this);
    buttonsTwoArgument[5] = new QPushButton("A<-->B");

    tastieraSecondArg->enableButtons(headerSecondMatrix->getRigheValue(),headerSecondMatrix->getColonneValue());
    tastieraFirstArg->enableButtons(headerFirstMatrix->getRigheValue(),headerFirstMatrix->getColonneValue());


    layoutHeaderContainer->addWidget(lFirst,0,0);
    layoutHeaderContainer->addWidget(lSecond,0,1);
    layoutHeaderContainer->addWidget(headerFirstMatrix,1,0);
    layoutHeaderContainer->addWidget(headerSecondMatrix,1,1);

    layoutMatrixContainer->addWidget(firstMatrix);
    layoutButtonsContainer->addWidget(buttonsTwoArgument[0]);
    layoutButtonsContainer->addWidget(buttonsTwoArgument[1]);
    layoutButtonsContainer->addWidget(buttonsTwoArgument[2]);
    layoutButtonsContainer->addWidget(buttonsTwoArgument[3]);
    layoutButtonsContainer->addWidget(buttonsTwoArgument[4]);
    layoutButtonsContainer->addWidget(buttonsTwoArgument[5]);
    layoutMatrixContainer->addLayout(layoutButtonsContainer);

    layoutMatrixContainer->addWidget(secondMatrix);
    layoutKeyboardContainer->addWidget(tastieraFirstArg);
    layoutKeyboardContainer->addWidget(tastieraSecondArg);
    layoutMatrixContainer->setSpacing(0);

    layoutResultContainer->addWidget(resultL);
    layoutResultContainer->addWidget(resultC);
    layoutResultContainer->addWidget(resultR);

    titolo->setStyleSheet("font-size: 15pt;");
    titolo->setStyleSheet("font-weight: bold;");
    resultC->hide();
    resultR->hide();

    l->addLayout(layoutHeaderContainer);
    l->addLayout(layoutMatrixContainer);
    l->addLayout(layoutKeyboardContainer);
    l->addWidget(titolo);
    l->addLayout(layoutResultContainer);
    l->setAlignment(titolo,Qt::AlignCenter);
    l->insertStretch(-1,1);

    setLayout(l);

    setFixedWidth(800);

    QPushButton*& ok1=headerFirstMatrix->getOkButton();
    QPushButton*& ok2=headerSecondMatrix->getOkButton();

    QPushButton*& elGaussJ1 = tastieraFirstArg->getButton(0);
    QPushButton*& elGaussJ2 = tastieraSecondArg->getButton(0);

    QPushButton*& decLU1 = tastieraFirstArg->getButton(1);
    QPushButton*& decLU2 = tastieraSecondArg->getButton(1);

    QPushButton*& trasposta1 = tastieraFirstArg->getButton(2);
    QPushButton*& trasposta2 = tastieraSecondArg->getButton(2);

    QPushButton*& determinante1 = tastieraFirstArg->getButton(3);
    QPushButton*& determinante2 = tastieraSecondArg->getButton(3);

    QPushButton*& inversa1 = tastieraFirstArg->getButton(4);
    QPushButton*& inversa2 = tastieraSecondArg->getButton(4);

    QPushButton*& rango1 = tastieraFirstArg->getButton(5);
    QPushButton*& rango2 = tastieraSecondArg->getButton(5);

    QPushButton*& norma1 = tastieraFirstArg->getButton(6);
    QPushButton*& norma2 = tastieraSecondArg->getButton(6);

    QPushButton*& normalz1 = tastieraFirstArg->getButton(7);
    QPushButton*& normalz2 = tastieraSecondArg->getButton(7);

    QPushButton*& elGauss1 = tastieraFirstArg->getButton(9);
    QPushButton*& elGauss2 = tastieraSecondArg->getButton(9);

    QPushButton*& prodottoSc1 = tastieraFirstArg->getButton(8);
    QPushButton*& prodottoSc2 = tastieraSecondArg->getButton(8);


    connect(ok1,&QPushButton::clicked,this,&MatrixGUI::settaDimensioni);
    connect(ok2,&QPushButton::clicked,this,&MatrixGUI::settaDimensioni);
    connect(trasposta1,&QPushButton::clicked,this,&MatrixGUI::trasponi);
    connect(trasposta2,&QPushButton::clicked,this,&MatrixGUI::trasponi);
    connect(elGaussJ1,&QPushButton::clicked,this,&MatrixGUI::elGaussJ);
    connect(elGaussJ2,&QPushButton::clicked,this,&MatrixGUI::elGaussJ);
    connect(decLU1,&QPushButton::clicked,this,&MatrixGUI::decPLU);
    connect(decLU2,&QPushButton::clicked,this,&MatrixGUI::decPLU);
    connect(determinante1,&QPushButton::clicked,this,&MatrixGUI::determinante);
    connect(determinante2,&QPushButton::clicked,this,&MatrixGUI::determinante);
    connect(inversa1,&QPushButton::clicked,this,&MatrixGUI::inversa);
    connect(inversa2,&QPushButton::clicked,this,&MatrixGUI::inversa);
    connect(rango1,&QPushButton::clicked,this,&MatrixGUI::rango);
    connect(rango2,&QPushButton::clicked,this,&MatrixGUI::rango);
    connect(norma1,&QPushButton::clicked,this,&MatrixGUI::norma);
    connect(norma2,&QPushButton::clicked,this,&MatrixGUI::norma);
    connect(normalz1,&QPushButton::clicked,this,&MatrixGUI::normalizzazione);
    connect(normalz2,&QPushButton::clicked,this,&MatrixGUI::normalizzazione);
    connect(prodottoSc1,&QPushButton::clicked,this,&MatrixGUI::prodottoScalare);
    connect(prodottoSc2,&QPushButton::clicked,this,&MatrixGUI::prodottoScalare);
    connect(elGauss1,&QPushButton::clicked,this,&MatrixGUI::eliminazioneGauss);
    connect(elGauss2,&QPushButton::clicked,this,&MatrixGUI::eliminazioneGauss);


    connect(buttonsTwoArgument[0],&QPushButton::clicked,this,&MatrixGUI::addizione);
    connect(buttonsTwoArgument[1],&QPushButton::clicked,this,&MatrixGUI::sottrazione);
    connect(buttonsTwoArgument[2],&QPushButton::clicked,this,&MatrixGUI::moltiplicazione);
    connect(buttonsTwoArgument[3],&QPushButton::clicked,this,&MatrixGUI::distanza);
    connect(buttonsTwoArgument[4],&QPushButton::clicked,this,&MatrixGUI::angolo);
    connect(buttonsTwoArgument[5],&QPushButton::clicked,this,&MatrixGUI::scambia);

    connect(resultL->getTrasferisci(),&QPushButton::clicked,this,&MatrixGUI::trasferisciRisultato);
    connect(resultC->getTrasferisci(),&QPushButton::clicked,this,&MatrixGUI::trasferisciRisultato);
    connect(resultR->getTrasferisci(),&QPushButton::clicked,this,&MatrixGUI::trasferisciRisultato);

}

//------------------------------------------
//           METODI DI UTILITA'
//------------------------------------------


bool MatrixGUI::matrixViewModified(bool mwPosition) const {
    if(mwPosition)
        return firstMatrix->getModifica();
    return secondMatrix->getModifica();
}

vector < vector< QLineEdit* > > MatrixGUI::getMatrixContent (bool mwPosition) const {
    if(mwPosition)
        return firstMatrix->getCelle();
    return secondMatrix->getCelle();
}

//Metodo settaDimensioni
void MatrixGUI::settaDimensioni() {
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());

    if(sender==headerFirstMatrix->getOkButton()){
        delete firstMatrix;
        firstMatrix = new MatrixView(this,headerFirstMatrix->getRigheValue(),headerFirstMatrix->getColonneValue());
        controller.buildOperand(firstMatrix->getCelle(),true);
        setViewPosition(true);
        return;
    }

    if(sender==headerSecondMatrix->getOkButton()) {
        delete secondMatrix;
        secondMatrix = new MatrixView(this,headerSecondMatrix->getRigheValue(),headerSecondMatrix->getColonneValue());
        controller.buildOperand(secondMatrix->getCelle(),false);
        setViewPosition(false);
        return;
    }
}

//Metodo stampaRis (stampa il risultato)
//se res == true allora si vuole stampare il risultato a sinistra, altrimenti a destra
void MatrixGUI::stampaRis(vector< vector<double> >ele, posRisultato res,QString title) {
    //eliminando i widget vengono eliminati anche i connect relativi ad essi, per cui
    //bisogna ristabilire i connect
    vector< vector<double> >el = controller.truncate(ele);
    if(res == posRisultato::left){
        if(resultC) {
           resultC->hide();
           delete resultC;
           resultC = 0;
        }
        if(resultR) {
            resultR->hide();
            delete resultR;
            resultR = 0;
        }

        delete resultL;
        resultL = new MatrixResult(this,el.size(),el[0].size());
        resultL->setTitle(title);
        titolo->setStyleSheet("font-size: 13pt;");
        connect(resultL->getTrasferisci(),&QPushButton::clicked,this,&MatrixGUI::trasferisciRisultato);
        for(int i = 0; i < resultL->getRighe(); ++i )
            for(int j = 0; j < resultL->getColonne(); ++j) {
                ((*resultL)[i][j])->setText(QString::number(el[i][j]));
            }

        layoutResultContainer->addWidget(resultL);
        return;
    }
    if(res == posRisultato::center) {
        if(resultC) {
           delete resultC;
           resultC = 0;
        }
        if(resultR) {
            delete resultR;
            resultR = 0;
        }
        resultC = new MatrixResult(this,el.size(),el[0].size());
        resultC->setTitle(title);
        connect(resultC->getTrasferisci(),&QPushButton::clicked,this,&MatrixGUI::trasferisciRisultato);
        for(int i = 0; i < resultC->getRighe(); ++i )
            for(int j = 0; j < resultC->getColonne(); ++j)
                ((*resultC)[i][j])->setText(QString::number(el[i][j]));

        layoutResultContainer->addWidget(resultC);
        return;
    }
    if(res == posRisultato::right) {
        if(resultR)
            delete resultR;
        resultR = new MatrixResult(this,el.size(),el[0].size());
        resultR->setTitle(title);

        connect(resultR->getTrasferisci(),&QPushButton::clicked,this,&MatrixGUI::trasferisciRisultato);
        for(int i = 0; i < resultR->getRighe(); ++i )
            for(int j = 0; j < resultR->getColonne(); ++j)
                ((*resultR)[i][j])->setText(QString::number(el[i][j]));

        layoutResultContainer->addWidget(resultR);
        return;
    }
}

//Metodo di stampa del risultato numerico
void MatrixGUI::stampaRisNumerico(double ris,QString testo) {
   QString messaggio = testo+"<br>"+QString::number(ris)+"</br>";
   QMessageBox::information(this,"Risultato",messaggio);
}

void MatrixGUI::stampaRisNumerici(QString messaggio) {
    QMessageBox::information(this,"Risultato",messaggio);
}

void MatrixGUI::displayMessaggioErrore(QString messaggio) {
    QMessageBox::critical(this,"Errore",messaggio);
}

//Metodo che sistema il layout
void MatrixGUI::setViewPosition(bool a){
    if(a) {
        layoutMatrixContainer->removeItem(layoutButtonsContainer);
        layoutMatrixContainer->removeWidget(secondMatrix);
        layoutMatrixContainer->addWidget(firstMatrix);
        layoutMatrixContainer->addLayout(layoutButtonsContainer);
        layoutMatrixContainer->addWidget(secondMatrix);
        tastieraFirstArg->enableButtons(firstMatrix->getRighe(),firstMatrix->getColonne());
    }
    else {
        layoutMatrixContainer->addWidget(secondMatrix);
        tastieraSecondArg->enableButtons(secondMatrix->getRighe(),secondMatrix->getColonne());
    }
}

//Metodo ausialiario per trasferire il risultato in uno dei due operandi (Matrice A o Matrice B)
void MatrixGUI::trasferisciRisAux(MatrixView*& target, MatrixResult*& toCopy) {
    delete target;
    target = new MatrixView(this,toCopy->getRighe(),toCopy->getColonne());
    //copio la rappresentazione del risultato
    for(int i = 0; i < toCopy->getRighe(); ++i )
        for(int j = 0; j < toCopy->getColonne(); ++j)
            ((*target)[i][j])->setText(((*toCopy)[i][j])->text());

    target->setModifica(false);
}

//Metodo di trasferimento del risultato
void MatrixGUI::trasferisciRisultato() {
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
    try{
        if(sender == resultL->getTrasferisci()) {
            int indexSelected = resultL->getIndexSelected();
            //trasferisco resultL
            if(indexSelected == 0) {
                trasferisciRisAux(firstMatrix,resultL);
                setViewPosition(true);
                controller.assignResultToOp(true,0);
                return;
            }

            trasferisciRisAux(secondMatrix,resultL);
            setViewPosition(false);
            controller.assignResultToOp(false,0);
            secondMatrix->setModifica(false);
            return;
        }
        if(sender == resultC->getTrasferisci()) {
            int indexSelected = resultC->getIndexSelected();
            if(indexSelected == 0) {
                trasferisciRisAux(firstMatrix,resultC);
                setViewPosition(true);
                controller.assignResultToOp(true,1);
                return;
            }

            trasferisciRisAux(secondMatrix,resultC);
            setViewPosition(false);
            controller.assignResultToOp(false,1);
            return;
        }
        if(sender == resultR->getTrasferisci()) {
            int indexSelected = resultR->getIndexSelected();
            if(indexSelected == 0) {
               trasferisciRisAux(firstMatrix,resultR);
                setViewPosition(true);
                controller.assignResultToOp(true,2);
                return;
            }

            trasferisciRisAux(secondMatrix,resultR);
            setViewPosition(false);
            controller.assignResultToOp(false,2);
            return;
        }
    }
    catch(controller_error& e){
        displayMessaggioErrore(e.what());
    }
}

//funzione che pulisce i risultati, o meglio li elimina
void MatrixGUI::resetResults() {
    layoutResultContainer->removeWidget(resultL);
    layoutResultContainer->removeWidget(resultC);
    layoutResultContainer->removeWidget(resultR);
    if(resultL) {
        delete resultL;
        resultL = 0;
    }
    if(resultC){
        delete resultC;
        resultC = 0;
    }
    if(resultR){
        delete resultR;
        resultR = 0;
    }
}

void MatrixGUI::scambia() {
    controller.switchM();
    layoutMatrixContainer->removeWidget(firstMatrix);
    layoutMatrixContainer->removeWidget(secondMatrix);
    MatrixView * aux = firstMatrix;
    firstMatrix = secondMatrix;
    secondMatrix = aux;
    aux = 0;
    setViewPosition(true);
    setViewPosition(false);
}

//-------------------------------------
//          METODI MATEMATICI
//-------------------------------------

void MatrixGUI::distanza() {
    try{
        controller.distance();
        firstMatrix->setModifica(false);
        secondMatrix->setModifica(false);
    }
    catch(controller_error& e) {
        displayMessaggioErrore(e.what());
    }
}

void MatrixGUI::trasponi() {
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());

    if(sender == tastieraFirstArg->getButton(2)) {
        controller.traspose(true);
        firstMatrix->setModifica(false);
    }
    else {
        controller.traspose(false);
        secondMatrix->setModifica(false);
    }
}


void MatrixGUI::inversa(){
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
    try{
        if(sender == tastieraFirstArg->getButton(4)){
            controller.inverse(true);
            firstMatrix->setModifica(false);
            return;
        }
        if(sender == tastieraSecondArg->getButton(4)) {
            controller.inverse(false);
            secondMatrix->setModifica(false);
        }
    }
    catch(controller_error& e){
        displayMessaggioErrore(e.what());
    }

}


void MatrixGUI::norma(){
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
    try{
        if(sender == tastieraFirstArg->getButton(6)){
            controller.norm(true);
            firstMatrix->setModifica(false);
        }
        else {
            controller.norm(false);
            secondMatrix->setModifica(false);
        }
    }
    catch(controller_error& e){
        displayMessaggioErrore(e.what());
    }
}

//Decomposizione LU
void MatrixGUI::decPLU(){
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
    //devo fare il display del secondo risultato
    if(sender == tastieraFirstArg->getButton(1)){
        controller.PLUdecomp(true);
        firstMatrix->setModifica(false);
        return;
    }
    if(sender == tastieraSecondArg->getButton(1)) {
        controller.PLUdecomp(false);
        secondMatrix->setModifica(false);
    }
}


void MatrixGUI::determinante(){
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());

    try{
        if(sender == tastieraFirstArg->getButton(3)) {
            controller.determinant(true);
            firstMatrix->setModifica(false);
        }
        else {
            controller.determinant(false);
            secondMatrix->setModifica(false);
        }
    }
    catch(controller_error& e){
        displayMessaggioErrore(e.what());
    }


}

void MatrixGUI::elGaussJ(){
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());

    if(sender == tastieraFirstArg->getButton(0)) {
        controller.GaussJordanElimination(true);
        firstMatrix->setModifica(false);
    }
    else {
        controller.GaussJordanElimination(false);
        secondMatrix->setModifica(false);
    }
}

void MatrixGUI::eliminazioneGauss() {
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());

    if(sender == tastieraFirstArg->getButton(9)) {
        controller.GaussElimination(true);
        firstMatrix->setModifica(false);
    }
    else {
        controller.GaussElimination(false);
        secondMatrix->setModifica(false);
    }
}

void MatrixGUI::rango(){
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
    if(sender == tastieraFirstArg->getButton(5)) {
        controller.rank(true);
        firstMatrix->setModifica(false);
        return;
    }
    controller.rank(false);
    secondMatrix->setModifica(false);  
}

void MatrixGUI::prodottoScalare() {
   QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
   if(sender == tastieraFirstArg->getButton(8)) {
       controller.scalarMultiply(tastieraFirstArg->getScalare(),true);
       firstMatrix->setModifica(false);
       return;
   }
   controller.scalarMultiply(tastieraSecondArg->getScalare(),false);
   secondMatrix->setModifica(false);
   return;
}

void MatrixGUI::moltiplicazione() {
    controller.multiply();
    firstMatrix->setModifica(false);
    secondMatrix->setModifica(false);

}

void MatrixGUI::addizione() {
    controller.add();
    firstMatrix->setModifica(false);
    secondMatrix->setModifica(false);
}

void MatrixGUI::sottrazione() {
    controller.subtract();
    firstMatrix->setModifica(false);
    secondMatrix->setModifica(false);
}

void MatrixGUI::angolo() {
    try{
        controller.angle();
        firstMatrix->setModifica(false);
        secondMatrix->setModifica(false);
    }
    catch(controller_error& e){
        displayMessaggioErrore(e.what());
    }

}

void MatrixGUI::normalizzazione() {
    QPushButton* sender = qobject_cast<QPushButton*>(QObject::sender());
    try{
        if(sender == tastieraFirstArg->getButton(7)) {
            controller.normalization(true);
            firstMatrix->setModifica(false);
            return;
        }
        controller.normalization(false);
        secondMatrix->setModifica(false);
    }
    catch(controller_error& e){
        displayMessaggioErrore(e.what());
    }

}

void MatrixGUI::setTitoloRisultato(QString tit) {
    titolo->setText(tit);
}

