#ifndef MATRIXGUI_H
#define MATRIXGUI_H

#include <QWidget>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QFrame>
#include <QTextEdit>
#include <QDoubleValidator>
#include <vector>
#include "matrixview.h"
#include "matrixdim.h"
#include "matrixresult.h"
#include "matrixtastiera.h"
#include "../../controller/matrix/matrixcontroller.h"
#include "abstractmatrixgui.h"

using std::vector;


class MatrixGUI: public abstractMatrixGUI {
    Q_OBJECT

private:
    MatrixDim *headerFirstMatrix;
    MatrixDim *headerSecondMatrix;
    MatrixView *firstMatrix;
    MatrixView *secondMatrix;
    QPushButton *buttonsTwoArgument[6];
    MatrixTastiera *tastieraFirstArg;
    MatrixTastiera *tastieraSecondArg;
    QVBoxLayout *l;
    QLabel *lFirst;
    QLabel *lSecond;
    MatrixResult* resultL;
    MatrixResult* resultC;
    MatrixResult* resultR;

    QHBoxLayout *layoutMatrixContainer;
    QVBoxLayout *layoutButtonsContainer;
    QGridLayout *layoutHeaderContainer;
    QHBoxLayout *layoutLabelContainer;
    QHBoxLayout *layoutKeyboardContainer;
    QHBoxLayout *layoutResultContainer;

    QLabel* titolo;

    MatrixController controller;


public:
    explicit MatrixGUI(QWidget* parent=0);
    void setViewPosition(bool);
    void resetResults();
    void stampaRis(vector<vector<double> >,posRisultato,QString = "");
    void stampaRisNumerici(QString);
    void stampaRisNumerico(double,QString);
    bool matrixViewModified(bool) const;
    vector < vector< QLineEdit* > > getMatrixContent (bool) const;
    void setTitoloRisultato(QString titolo);
    void displayMessaggioErrore(QString);
    void trasferisciRisAux(MatrixView*& , MatrixResult*&);
signals:

public slots:
    void addizione();
    void sottrazione();
    void inversa();
    void decPLU();
    void elGaussJ();
    void eliminazioneGauss();
    void norma();
    void distanza();
    void scambia();
    void determinante();
    void rango();
    void settaDimensioni();
    void moltiplicazione();
    void trasponi();
    void trasferisciRisultato();  
    void prodottoScalare();
    void normalizzazione();
    void angolo();
};

#endif // MATRIXGUI_H
