#ifndef MATRIXTASTIERA_H
#define MATRIXTASTIERA_H

#include <QWidget>
#include <QPushButton>
#include <vector>
#include <QGridLayout>
#include <QLineEdit>
#include <QDoubleValidator>

class MatrixTastiera : public QWidget
{
    Q_OBJECT
private:
    std::vector<QPushButton*> buttons;
    QGridLayout *layout;
    QDoubleValidator* validatore;
    QLineEdit *prodotto;

    void setEnabled(bool);
public:
    explicit MatrixTastiera(QWidget *parent = 0);
    QPushButton*& getButton(int);
    void enableButtons(int righe, int colonne);
    double getScalare() const;
signals:

public slots:
};

#endif // MATRIXTASTIERA_H
