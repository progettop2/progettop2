#ifndef ABSTRACTMATRIXGUI_H
#define ABSTRACTMATRIXGUI_H
#include <QWidget>
#include <QString>
#include <QLineEdit>
#include <vector>
#include "../abstractgui.h"
using std::vector;

enum class posRisultato {left,center,right};

class abstractMatrixGUI: public abstractGUI {
    Q_OBJECT
public:
    explicit abstractMatrixGUI(QWidget* parent = 0):abstractGUI(parent) {}
    virtual vector < vector< QLineEdit* > > getMatrixContent (bool) const = 0;
    virtual void stampaRis(vector< vector<double> >, posRisultato,QString = "") = 0;
    virtual void stampaRisNumerici(QString) = 0;
    virtual void stampaRisNumerico(double,QString) = 0;
    virtual bool matrixViewModified(bool) const = 0;
    virtual void setTitoloRisultato(QString) = 0;
    virtual ~abstractMatrixGUI() {}
public slots:
    //Definisco gli slots che ogni GUI di una calcolatrice di matrice dovrebbe avere (almeno quelle fondamentali)
    virtual void addizione() = 0;
    virtual void sottrazione()= 0;
    virtual void inversa()= 0;
    virtual void determinante() = 0;
    virtual void trasponi() = 0;
    virtual void moltiplicazione() = 0;
    virtual void rango() = 0;

};
#endif // ABSTRACTMATRIXVIEW_H
