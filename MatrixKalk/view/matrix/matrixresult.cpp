#include "matrixresult.h"

MatrixResult::MatrixResult(QWidget *parent, unsigned int r,unsigned int c) : QWidget(parent),trasferisci(new QPushButton("OK")),
    selezione(new QComboBox),layout(new QVBoxLayout),title(new QLabel), righe(r),colonne(c),result(new MatrixView(this,righe,colonne)) {

    layout->addWidget(title);
    layout->addWidget(result);
    layout->addWidget(selezione);
    layout->addWidget(trasferisci);

    title->setStyleSheet("font-size: 10pt;");
    title->setStyleSheet("font-weight: bold;");
    layout->setAlignment(title, Qt::AlignCenter);
    layout->setAlignment(result, Qt::AlignCenter);
    layout->setAlignment(selezione, Qt::AlignCenter);
    layout->setAlignment(trasferisci, Qt::AlignCenter);

    selezione->addItem("trasferisci in A");
    selezione->addItem("trasferisci in B");
    selezione->adjustSize();
    trasferisci->setFixedSize(100,25);
    result->setReadOnly(true);

    setLayout(layout);
}

void MatrixResult::setTitle(QString titolo) {
    title->setText(titolo);
}
QPushButton*& MatrixResult::getTrasferisci() {
    return trasferisci;
}

QString MatrixResult::getSelezione() const{
    return selezione->currentText();
}

void MatrixResult::disableOk(bool a){
    trasferisci->setDisabled(a);
}


int MatrixResult::getIndexSelected() const {
    return selezione->currentIndex();
}

vector< vector<QLineEdit*> > MatrixResult::getElementi() const {
    return result->getCelle();
}

int MatrixResult::getColonne() const{

    return result->getColonne();
}

int MatrixResult::getRighe() const{

    return result->getRighe();
}

vector<QLineEdit*>& MatrixResult::operator[](const int i) {
    return (*result)[i];
}
