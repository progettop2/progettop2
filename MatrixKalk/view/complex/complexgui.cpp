#include "complexgui.h"
#include "../../controller/complex/complexcontroller.h"
#include <QDebug>
ComplexGUI::ComplexGUI(QWidget *parent) : abstractComplexGui(parent),history(new QTextEdit),labelDisplay(new QLabel("i")), displayR(new ComplexDisplay),displayI(new ComplexDisplay),
    tastiera(new ComplexTastiera(this)),displayUsato(ComplexT::reale),layoutGenerale(new QVBoxLayout),layoutDisplay(new QGridLayout),c(this), validator(new QDoubleValidator(this)) {

    setFixedSize(300,475);

    // set delle dimensioni e dei parametri desiderati
    history->setFixedHeight(50);
    history->setStyleSheet("margin-bottom: 5px;");
    history->setReadOnly(true);
    labelDisplay->setStyleSheet("font-size: 15pt;");

    validator->setDecimals(3);

    displayR->setFixedSize(130,30);
    displayR->setText("0");
    displayR->setPlaceholderText("0");
    displayI->setPlaceholderText("0");
    displayI->setFixedSize(115,30);

    displayR->setAlignment(Qt::AlignRight);
    displayI->setAlignment(Qt::AlignRight);

    //assegno il validator di double ai display
    displayR->setValidator(validator);
    displayI->setValidator(validator);

    //aggiunta dei widget ai layout
    layoutDisplay->addWidget(displayR,0,0);
    layoutDisplay->addWidget(new QLabel("+"),0,3);
    layoutDisplay->addWidget(displayI,0,4);
    layoutDisplay->addWidget(labelDisplay,0,5);
    layoutGenerale->addWidget(history);
    layoutGenerale->addLayout(layoutDisplay);
    layoutGenerale->addWidget(tastiera);

    //assegnazione del layout del padre
    setLayout(layoutGenerale);

    //connect dei tasti
    connect(tastiera->getButton(0),&QPushButton::clicked,this,&ComplexGUI::coniugato);
    connect(tastiera->getButton(1),&QPushButton::clicked,this,&ComplexGUI::argomento);
    connect(tastiera->getButton(2),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(3),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(4),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(5),&QPushButton::clicked,this,&ComplexGUI::pow);
    connect(tastiera->getButton(6),&QPushButton::clicked,this,&ComplexGUI::abs);
    connect(tastiera->getButton(7),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(8),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(9),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(10),&QPushButton::clicked,this,&ComplexGUI::moltiplicazione);
    connect(tastiera->getButton(11),&QPushButton::clicked,this,&ComplexGUI::divisione);
    connect(tastiera->getButton(12),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(13),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(14),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(15),&QPushButton::clicked,this,&ComplexGUI::addizione);
    connect(tastiera->getButton(16),&QPushButton::clicked,this,&ComplexGUI::sottrazione);
    connect(tastiera->getButton(17),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(18),&QPushButton::clicked,this,&ComplexGUI::inserisciNumero);
    connect(tastiera->getButton(19),&QPushButton::clicked,this,&ComplexGUI::esegui);
    connect(tastiera->getButton(20),&QPushButton::clicked,this,&ComplexGUI::del);
    connect(tastiera->getButton(21),&QPushButton::clicked,this,&ComplexGUI::ac);
    connect(tastiera->getButton(22),&QPushButton::clicked,this,&ComplexGUI::setDisplay);
    connect(tastiera->getButton(23),&QPushButton::clicked,this,&ComplexGUI::root);
    connect(displayR,&ComplexDisplay::focussed,this,&ComplexGUI::setDisplay);
    connect(displayI,&ComplexDisplay::focussed,this,&ComplexGUI::setDisplay);
    connect(displayR,&ComplexDisplay::textChanged,this,&ComplexGUI::inputDaTastiera);
    connect(displayI,&ComplexDisplay::textChanged,this,&ComplexGUI::inputDaTastiera);
    connect(displayR,&ComplexDisplay::numberPressed,this,&ComplexGUI::inputNuovaOperazione);
    connect(displayI,&ComplexDisplay::numberPressed,this,&ComplexGUI::inputNuovaOperazione);
    connect(displayR,&ComplexDisplay::del,this,&ComplexGUI::del);
    connect(displayI,&ComplexDisplay::del,this,&ComplexGUI::del);
    connect(displayR,&ComplexDisplay::operandPressed,this,&ComplexGUI::gestioneOperazioni);
    connect(displayI,&ComplexDisplay::operandPressed,this,&ComplexGUI::gestioneOperazioni);

    c.setAppenaEseguito(true);
}

/**
 * @brief ComplexGUI::inputDaTastiera gestisce l'input da tastiera
 * @param a contenuto del tasto premuto
 */
void ComplexGUI::inputDaTastiera(const QString& a) {

    QLineEdit* display = qobject_cast<QLineEdit*>(sender());
    if(display==displayI)
       c.updateComplesso(ComplexT::immaginaria,a);
    if(display==displayR)
       c.updateComplesso(ComplexT::reale,a);

}

void ComplexGUI::inputNuovaOperazione(QString text) {
    QObject* s= sender();
    if(c.getAppenaEseguito()){
        c.setAppenaEseguito(false);
        if(s == displayI) {
            displayI->setText(text);
            displayR->clear();
        }
        if(s ==displayR) {
            displayR->setText(text);
            displayI->clear();
         }
    }else{
        if(s == displayI)
            displayI->setText(displayI->text()+text);
        if(s == displayR)
            displayR->setText(displayR->text()+text);

    }
}

/**
 * @brief ComplexGUI::inserisciNumeroslot chiamato alla pressione di un tasto sul tastierino a schermo
 */
void ComplexGUI::inserisciNumero() {

    QPushButton* bottone = qobject_cast<QPushButton*>(sender());
    c.inserisciNumero(bottone);

}

/**
 * @brief ComplexGUI::setDisplay alla pressione di tab, imposta il display corrente
 */
void ComplexGUI::setDisplay() {

    QObject *obj=sender();
        if(obj==displayR){
            displayUsato=ComplexT::reale;
            return;
        }
        if(obj==displayI) {
            displayUsato=ComplexT::immaginaria;
            return;
        }


    if(displayUsato == ComplexT::reale)
        displayUsato = ComplexT::immaginaria;
    else
        displayUsato = ComplexT::reale;

}
/**
 * @brief ComplexGUI::del richiede al controller di gestire la pressione del tasto del
 */
void ComplexGUI::del() {

    c.del();
}



/**
 * @brief ComplexGUI::setTextI imposta il testo del display contenente la parte immaginaria
 * @param text stringa da assegnare al display
 */
void ComplexGUI::setTextI(QString text) {

    displayI->setText(text);
}


/**
 * @brief ComplexGUI::setTextR imposta il testo del display contenente la parte reale
 * @param text stringa da assegnare al display
 */
void ComplexGUI::setTextR(QString text) {

    displayR->setText(text);
}
/**
 * @brief ComplexGUI::getTextI
 * @return contenuto del display della parte immaginaria
 */
QString ComplexGUI::getTextI() const {

    return displayI->text();
}


/**
 * @brief ComplexGUI::getTextR
 * @return contenuto del display della parte reale
 */
QString ComplexGUI::getTextR() const {

    return displayR->text();
}

/**
 * @brief ComplexGUI::getDisplayUsato
 * @return display attualmente utilizzato
 */
ComplexT ComplexGUI::getDisplayUsato() const {

    return displayUsato;
}

/**
 * @brief ComplexGUI::clear cancella contenuto dei display e della cronologia
 */
void ComplexGUI::clear() {

    displayR->setText("");
    displayI->setText("");
    history->clear();
}

/**
 * @brief ComplexGUI::keyPressEvent overloading della funzione che consente di gestire la pressione dei tasti enter
 * @param event
 */
void ComplexGUI::keyPressEvent(QKeyEvent *event) {

    if(event->key()== Qt::Key_Enter||event->key()== Qt::Key_Return){
        if(tastiera->getButton(19)->isEnabled())
            esegui();
    }else {
        gestioneOperazioni(event->text());
    }
}

void ComplexGUI::gestioneOperazioni(QString text) {


    if(text== "*") {
        moltiplicazione();
    }

    if(text== "+") {
        addizione();
    }

    if(text== "/") {
        divisione();
    }


}





/**
 * @brief ComplexGUI::enableDisplay abilita o disabilita i display
 * @param a true se si vuole abilitare, false altrimenti
 */
void ComplexGUI::enableDisplay(bool a){

    displayI->setEnabled(a);
    displayR->setEnabled(a);
}

/**
 * @brief ComplexGUI::getExecuteButton permette di accedere al tasto esegui,in modo che il controller lo possa gestire
 * @return QPushButton*& del tasto =
 */
QPushButton*& ComplexGUI::getExecuteButton() {

    return tastiera->getButton(19);

}
/**
 * @brief ComplexGUI::root chiede al controller di restituire le radici di un complesso in modo che siano leggibili
 */
void ComplexGUI::root() {

    QString text=c.root(tastiera->getSpinBox()->value());
    history->setText(text);
}

/**
 * @brief ComplexGUI::pow slot che chiede al controller di preparare la funzione di potenza di un complesso
 */
void ComplexGUI::pow() {

    QString op=checkNegativo();
    QString i=checkImgNull();

    if(displayI->text()!="0")
        history->setText(displayR->text()+op+displayI->text()+i+" ^");
    else
        history->setText(displayR->text()+" ^");

    displayI->setDisabled(true);
    c.preparaOperazione(4);
    displayR->setText("0");
    displayI->setText("0");
    displayR->setFocus();
    c.setOperazioneRichiesta(4);
    return;
}
/**
 * @brief ComplexGUI::abs slot che chiede al controller di preparare la funzione di modulo di un complesso
 */
void ComplexGUI::abs() {

    QString op=checkNegativo();
    QString i=checkImgNull();
    if(displayI->text()!="0")
        history->setText("|"+displayR->text()+op+displayI->text()+i+"|");
    else
        history->setText("|"+displayR->text()+"|");
    c.preparaOperazione(3);
    c.setOperazioneRichiesta(3);

    c.modulo();
    history->append(c.restituisciRisultato(displayR->text(),displayI->text()));
    return;
}
/**
 * @brief ComplexGUI::ac chiede al controller di cancellare tutti gli elementi presenti nel display e nella cronologia
 */
void ComplexGUI::ac() {

    c.ac();
    history->setText("");
}
/**
 * @brief ComplexGUI::moltiplicazione chiede al controller di preparare l'operazione di moltiplicazione
 */
void ComplexGUI::moltiplicazione() {
    QString op=checkNegativo();
    QString i=checkImgNull();
    if(displayI->text()!="0")
        history->setText(displayR->text()+op+displayI->text()+i+" *");
    else
        history->setText(displayR->text()+" *");

    c.preparaOperazione(5);
    c.setOperazioneRichiesta(5);
    return;
}

/**
 * @brief ComplexGUI::addizione chiede al controller di preparare l'operazione di adddizione
 */
void ComplexGUI::addizione() {

    QString op=checkNegativo();
    QString i=checkImgNull();
    if(displayI->text()!="0")
        history->setText(displayR->text()+op+displayI->text()+i+" +");
    else
        history->setText(displayR->text()+" +");

    c.preparaOperazione(6);
    c.setOperazioneRichiesta(6);
    return;
}

/**
 * @brief ComplexGUI::divisione chiede al controller di preparare l'operazione di divisione
 */
void ComplexGUI::divisione() {

    QString op=checkNegativo();
    QString i=checkImgNull();
    if(displayI->text()!="0")
        history->setText(displayR->text()+op+displayI->text()+i+" /");
    else
        history->setText(displayR->text()+" /");

    c.preparaOperazione(7);
    c.setOperazioneRichiesta(7);
    return;
}

/**
 * @brief ComplexGUI::sottrazione chiede al controller di preparare l'operazione di sottrazione
 */
void ComplexGUI::sottrazione() {

    QString op=checkNegativo();
    QString i=checkImgNull();
    if(displayI->text()!="0")
        history->setText(displayR->text()+op+displayI->text()+i+" -");
    else
        history->setText(displayR->text()+" -");

    c.preparaOperazione(8);
    c.setOperazioneRichiesta(8);
    return;
}

/**
 * @brief ComplexGUI::esegui esegue l'operazione desiderata
 */
void ComplexGUI::esegui() {

        QString op=checkNegativo();
        QString i=checkImgNull();
        if(displayI->text()!="0")
            history->append(displayR->text()+op+displayI->text()+i+" =");
        else
            history->append(displayR->text()+" =");

        c.esegui();

        history->append(c.restituisciRisultato(displayR->text(),displayI->text()));

   return;
}

/**
 * @brief ComplexGUI::coniugato prepara l'operazione di coniugato
 */
void ComplexGUI::coniugato() {
    QString op=checkNegativo();
    QString i=checkImgNull();
    if(displayI->text()!="0")
        history->setText(displayR->text()+op+displayI->text()+i+" =");
    else
        history->setText(displayR->text()+" =");

    c.preparaOperazione(1);
    c.setOperazioneRichiesta(1);
    c.coniugato();
    history->append(c.restituisciRisultato(displayR->text(),displayI->text()));
    return;

}

/**
 * @brief ComplexGUI::argomento prepara l'operazione di argomento
 */
void ComplexGUI::argomento() {
    QString op=checkNegativo();
    QString i=checkImgNull();

    if(displayI->text()!="0")
        history->setText("arg("+displayR->text()+op+displayI->text()+i+") =");
    else
        history->setText("arg("+displayR->text()+") =");

    c.preparaOperazione(2);
    c.setOperazioneRichiesta(2);
    c.argomento();
    history->append(c.restituisciRisultato(displayR->text(),displayI->text()));

    return;
}

/**
 * @brief ComplexGUI::displayMessaggioErrore Permette di visualizzare un messaggio di errore a schermo
 * @param messaggio
 */
void ComplexGUI::displayMessaggioErrore(QString messaggio) {

    QMessageBox::critical(this,"Errore",messaggio);

}

QString ComplexGUI::checkNegativo() {
    QString op="+";
    if(displayI->text().toDouble()<=0)
        op="";
return op;
}



QString ComplexGUI::checkImgNull() {
    QString i="i";
    if(displayI->text().toDouble()==0)
        i="";
return i;


}
