#ifndef COMPLEXDISPLAY_H
#define COMPLEXDISPLAY_H
#include<QLineEdit>
#include<QFocusEvent>

class ComplexDisplay: public QLineEdit
{
    Q_OBJECT
public:
    ComplexDisplay(QWidget* parent=0);
    ~ComplexDisplay() {}
    virtual void focusInEvent(QFocusEvent *e);
    virtual void focusOutEvent(QFocusEvent *e);
    void keyPressEvent(QKeyEvent *);

signals:
   void focussed(bool);
   void numberPressed(QString);
   void operandPressed(QString);
   void del();
};

#endif // COMPLEXDISPLAY_H

