#ifndef COMPLEXGUI_H
#define COMPLEXGUI_H

#include "../../controller/complex/complexcontroller.h"
#include <QGridLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QDoubleValidator>
#include <QMessageBox>
#include <QDialogButtonBox>
#include "complextastiera.h"
#include "complexdisplay.h"
#include "abstractcomplexgui.h"


class ComplexGUI : public abstractComplexGui {
    Q_OBJECT
private:

    QTextEdit* history;
    QLabel *labelDisplay;
    ComplexDisplay *displayR;
    ComplexDisplay *displayI;
    ComplexTastiera *tastiera;
    ComplexT displayUsato;
    QVBoxLayout *layoutGenerale;
    QGridLayout *layoutDisplay;
    ComplexController c;
    QDoubleValidator *validator;
    QString checkNegativo();
    QString checkImgNull();

public:

    explicit ComplexGUI(QWidget *parent = 0);
    void setTextR(QString);
    void setTextI(QString);
    QString getTextR() const;
    QString getTextI() const;
    ComplexT getDisplayUsato() const;
    void clear();
    void keyPressEvent(QKeyEvent*);
    void enableDisplay(bool);
    QPushButton*& getExecuteButton();


signals:

public slots:

    void inputDaTastiera(const QString&);
    void inserisciNumero();
    void setDisplay();
    void del();
    void ac();
    void coniugato();
    void argomento();
    void abs();
    void pow();
    void moltiplicazione();
    void addizione();
    void divisione();
    void sottrazione();
    void esegui();
    void root();
    void displayMessaggioErrore(QString);
    void inputNuovaOperazione(QString);
    void gestioneOperazioni(QString);

};

#endif // COMPLEXGUI_H
