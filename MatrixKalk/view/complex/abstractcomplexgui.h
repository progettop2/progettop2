#ifndef ABSTRACTCOMPLEXGUI_H
#define ABSTRACTCOMPLEXGUI_H
#include <QWidget>
#include <QString>
#include <QPushButton>
#include <vector>
#include "../abstractgui.h"
using std::vector;
enum class ComplexT{reale,immaginaria};
class abstractComplexGui: public abstractGUI {
    Q_OBJECT
public:
    explicit abstractComplexGui(QWidget* parent = 0):abstractGUI(parent) {}
    virtual ~abstractComplexGui() {}
    virtual ComplexT getDisplayUsato()const =0;
    virtual void setTextR(QString)=0;
    virtual void setTextI(QString)=0;
    virtual QString getTextR()const =0;
    virtual QString getTextI()const =0;
    virtual void clear() =0;
    virtual void enableDisplay(bool)=0;
    virtual QPushButton*& getExecuteButton() = 0;
signals:

public slots:
    virtual void divisione() = 0;
};
#endif // ABSTRACTCOMPLEXGUI_H
