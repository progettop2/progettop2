#include "complexdisplay.h"
#include <QDebug>

ComplexDisplay::ComplexDisplay(QWidget *parent): QLineEdit(parent) {}


void ComplexDisplay::focusInEvent(QFocusEvent *e) {
  QLineEdit::focusInEvent(e);
  emit(focussed(true));
}

void ComplexDisplay::focusOutEvent(QFocusEvent *e) {
  QLineEdit::focusOutEvent(e);
  emit(focussed(false));
}

void ComplexDisplay::keyPressEvent(QKeyEvent *event) {

    if((event->key()== Qt::Key_0)||(event->key()== Qt::Key_1)||(event->key()== Qt::Key_2)||(event->key()== Qt::Key_3)||
            (event->key()== Qt::Key_7)||(event->key()== Qt::Key_6)||(event->key()== Qt::Key_5)||(event->key()== Qt::Key_4)||
            (event->key()== Qt::Key_8)||(event->key()== Qt::Key_9)||(event->key()== Qt::Key_Minus)||(event->text()==".")) {
        emit numberPressed(event->text());
    }

    if(event->key() == Qt::Key_Backspace) {
        emit del();
    }

    if((event->key()==Qt::Key_Asterisk)||(event->key()==Qt::Key_Minus)||(event->key()==Qt::Key_Slash)||(event->key()==Qt::Key_Plus))
        emit operandPressed(event->text());


}
