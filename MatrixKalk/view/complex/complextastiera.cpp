#include "complextastiera.h"

ComplexTastiera::ComplexTastiera(QWidget *parent) : QFrame(parent), root(new QSpinBox),layout(new QGridLayout) {
    setFixedSize(280,350);

    setFrameShape(QFrame::StyledPanel);
    setFrameShadow(QFrame::Raised);

    for(int i=0; i < 24; i++){
        QPushButton *b = new QPushButton(this);
        buttons.push_back(b);
    }

    layout->setContentsMargins(3,0,0,0);
    root->setMinimum(1);
    root->setWrapping(true);

    buttons[0]->setText("Coniugato");
    buttons[1]->setText("Argomento");
    buttons[2]->setText(QString::number(7));
    buttons[3]->setText(QString::number(8));
    buttons[4]->setText(QString::number(9));
    buttons[5]->setText("a^x");
    buttons[6]->setText("Abs");
    buttons[7]->setText(QString::number(4));
    buttons[8]->setText(QString::number(5));
    buttons[9]->setText(QString::number(6));
    buttons[10]->setText("*");
    buttons[11]->setText("/");
    buttons[12]->setText(QString::number(1));
    buttons[13]->setText(QString::number(2));
    buttons[14]->setText(QString::number(3));
    buttons[15]->setText("+");
    buttons[16]->setText("-");
    buttons[17]->setText(QString::number(0));
    buttons[18]->setText(".");
    buttons[19]->setText("=");
    buttons[20]->setText("DEL");
    buttons[21]->setText("AC");
    buttons[22]->setText("Tab");
    buttons[23]->setText("Radici");

    layout->addWidget(buttons[22],0,0);
    layout->addWidget(buttons[23],0,1);
    layout->addWidget(root,0,2);
    layout->addWidget(buttons[20],0,3);
    layout->addWidget(buttons[21],0,4);
    layout->addWidget(buttons[0],1,0,1,3);
    layout->addWidget(buttons[1],1,3,1,2);

    int row=2;
    int counter=2;
    for(int numberOfButtons=0; numberOfButtons <= 3; numberOfButtons++)
    {
        for (int f2=0; f2 < 5 && counter <= 16; f2++)
        {
            layout->addWidget(buttons[counter],row, f2);
            counter++;
        }
        row++;
    }
    layout->addWidget(buttons[17],5,0,1,3);
    layout->addWidget(buttons[18],5,3);
    layout->addWidget(buttons[19],5,4,1,2);


    for(int i=0; i <= 10; i=i+5) {
        for (int j=2; j <= 6; j++) {
            if(j<=4)
                buttons[i+j]->setFixedSize(50,45);
            else
                buttons[i+j]->setFixedSize(50,55);
        }
    }

    buttons[18]->setFixedWidth(50);
    buttons[19]->setFixedWidth(50);
    buttons[19]->setDisabled(true);



    setLayout(layout);
}

QPushButton*& ComplexTastiera::getButton(int i) {
    return buttons[i];
}


QSpinBox*& ComplexTastiera::getSpinBox() {
    return root;
}
