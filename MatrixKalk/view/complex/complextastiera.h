#ifndef COMPLEXTASTIERA_H
#define COMPLEXTASTIERA_H

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QFrame>
#include <QSpinBox>
#include <vector>
class ComplexTastiera : public QFrame{
    Q_OBJECT
private:
    std::vector<QPushButton*> buttons;
    QSpinBox *root;
    QGridLayout *layout;
public:
    explicit ComplexTastiera(QWidget *parent = 0);
    QPushButton*& getButton(int);
    QSpinBox*& getSpinBox();
signals:

public slots:
};

#endif // COMPLEXTASTIERA_H
