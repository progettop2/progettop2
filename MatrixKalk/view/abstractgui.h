#ifndef ABSTRACTGUI_H
#define ABSTRACTGUI_H
#include <QWidget>
#include <QString>

class abstractGUI: public QWidget {
    Q_OBJECT
public:
    explicit abstractGUI(QWidget* parent = 0): QWidget(parent) {}
    virtual void displayMessaggioErrore(QString) = 0;
    virtual ~abstractGUI() {}
signals:

public slots:
    virtual void addizione() = 0;
    virtual void sottrazione() = 0;
    virtual void moltiplicazione() = 0;


};
#endif // ABSTRACTGUI_H
