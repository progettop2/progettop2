#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTabWidget>
#include <QWidget>
#include <QScrollBar>
#include <QScrollArea>
#include "view/matrix/matrixgui.h"
#include "view/complex/complexgui.h"
class MainWindow : public QMainWindow {
private:
    MatrixGUI *mg;
    ComplexGUI *cg;
    QTabWidget *wg;
    QHBoxLayout *layoutComplessi;
    QWidget *windowComplessi;
    QScrollArea *scroll;
public:
    explicit MainWindow(QWidget* parent =0);


public slots:
}
;

#endif // MAINWINDOW_H
